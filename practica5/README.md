# Práctica 5

Verificación formal: chequeo de modelos acotados e inducción en el tiempo.

## Contenidos

[TOC]

# Trabajo previo

El alumno debería repasar el tema de verificación formal antes de esta práctica, repasando qué son las propiedades y suposiciones de un circuito y cómo se describen. También debería tener algún diseño VHDL sencillo, sintetizable y sin errores de sintaxis sobre el que poder trabajar

# Documentación importante

Además del tema de verificación formal, estará bien que tengan a mano dos de los elementos de la bibliografía del tema:

- T. Meissner, [PSL examples for GHDL and symbiosys](https://github.com/tmeissner/psl_with_ghdl)
- YosysHQ, [sby file format reference](https://symbiyosys.readthedocs.io/en/latest/reference.html#)

# Objetivos

El objetivo de esta práctica es proporcionar un ejemplo de herramienta de verificación formal SymbiYosys (`sby`) por si los alumnos quieren aplicar verificación formal a algunos elementos del trabajo desarrollado.

La aplicación de estas técnicas es opcional para el trabajo de la asignatura, aunque sube nota.

# Ejemplo

Se proporciona un ejemplo de verificación formal sobre un contador de 8 bits. Las propiedades y suposiciones sobre el circuito se encuentran en el propio fichero VHDL, [contador.vhd](practica5/contador.vhd)

El fichero `counter.sby` contiene las instrucciones para SymbiYosys. El fichero define una serie de tareas, pero si no define la sección `[tasks]`, se comportará como si existiera una única tarea que se ejecutará con las opciones indicadas en la sección `[options]`.

El `Makefile` lanza las distintas tareas, invocando a `sby` y pasándole como argumento el nombre de las mismas. Si no se especifica ninguna tarea a `sby`, se ejecutarán todas las tareas definidas en el fichero.

# Secuencias y abort

Para el interpolador y otros bloques, les puede ser útil definir secuencias. Definir secuencias nos permiten simplificar nuestras propiedades, así como reutilizar código cuando la misma secuencia aparece en diferentes propiedades. Por ejemplo, tras recibir una entrada válida, el interpolador mantendrá ``estim_valid`` activo durante 12 ciclos, lo que podemos expresar de la siguiente manera:

```vhdl
sequence twelve_estim_valid is {estim_valid[*12]};
```

Con esto, podemos intentar demostrar la propiedad de que siempre que se activa ``valid`` a la entrada, vamos a ver la secuencia definida anteriormente:

```vhdl
valid_outputs_after_valid_input:                                            
    assert always (({valid} |=> twelve_estim_valid) abort rst); 
```

La palabra reservada ``abort`` les permite indicar de manera sencilla que la propiedad no debe cumplirse si el circuito está en reset o es reseteado durante el funcionamiento.

# Trabajo del alumno

Se recomienda cambiar las propiedades y suposiciones proporcionadas en el ejemplo, para ver cómo reacciona la herramienta y qué clase de contraejemplos puede encontrar.

Tras esto, el siguiente paso sería escoger el diseño más sencillo que tengan en su trabajo, escribir algunas propiedades y suposiciones, y lanzar la herramienta de verificación formal.

Se pueden utilizar los conceptos vistos en teoría para demostrar propiedades de los diseños realizados por el alumno para el trabajo de la asignatura. Por ejemplo, en la FSM se podría probar que el PRBS es habilitado en ráfagas de 12 ciclos, o que, tras procesar un símbolo completo, la FSM vuelve a un estado (incluyendo todas las señales necesarias, no sólo la señal ``state``) en el cual el siguiente símbolo puede ser procesado.


