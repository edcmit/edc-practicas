library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fsm is
    port (
      rst    : in std_logic;
      clk    : in std_logic;
      up     : in std_logic;
      down   : in std_logic;
      speed  : out unsigned(1 downto 0)
    );
end fsm;

architecture fsm_arch of fsm is

  -- Define the datatype for the FSM state. Only the values in the list are
  -- valid values for the datatype
  type state_t is (idle, slow, medium, fast);

  -- Declare two signals, for the current and next state
  -- The signals use the datatype we have just defined
  signal state, p_state : state_t;

begin

  -- The synchronous process is the same as always, we have just to make sure
  -- that the reset value is a valid value for the datatype
  sync: process(rst, clk)
  begin
    if rst = '1' then
      state <= idle;  -- Cannot use 0, '0', (others=>'0') or similar values here: must be a valid state_t value
    elsif rising_edge(clk) then
      state <= p_state;
    end if;
  end process;

  -- The combinatorial process uses also the same statements as other
  -- combinatorial processes (if..elsif...else and case..when), but,
  -- for readability, it is very advisable to first use a case..when to
  -- determine in which state we are, and then for each state we add different
  -- logic in the form of if..elsif..else statemente
  comb: process(state, up, down)
  begin
    -- Before the 'case..when', it's a good practice to assign default values,
    -- so we avoid writing the same assignment in multiple places. For example,
    -- we want to maintain the state by default, so afterwards we will only
    -- assign it when we want it to change. This also avoids latches for any
    -- signals that are asigned before the 'case..when'
    p_state <= state;

    -- With the case statement we determine in which state we are, and inside
    -- each 'when' we write the logic for each state
    case state is
      when idle =>
        -- Logic for when we are in the idle state
        speed <= to_unsigned(0,2);
        if up = '1' then
          p_state <= slow;
        end if;
      when slow =>
        -- Logic for when we are in the slow state
        speed <= to_unsigned(1,2);
        if up = '1' then
          p_state <= medium;
        elsif down = '1' then
          p_state <= idle;
        end if;
      when medium =>
        -- Logic for when we are in the medium state
        speed <= to_unsigned(2,2);
        if up = '1' then
          p_state <= fast;
        elsif down = '1' then
          p_state <= slow;
        end if;
      when fast =>
        -- Logic for when we are in the fast state
        speed <= to_unsigned(3,2);
        if down = '1' then
          p_state <= fast;
        end if;
    end case;
  end process;

end fsm_arch;
