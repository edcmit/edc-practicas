library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_fsm is
end tb_fsm;

architecture tb_fsm_arch of tb_fsm is

  -- Signals for the UUT
  signal clk, rst, up, down : std_logic;
  signal speed : unsigned(1 downto 0);

  -- Simulation control
  constant NCYCLES    : integer := 20;
  constant clk_period : time    := 10 ns;
  signal endsim       : boolean := false;

begin

  -- Instance of the UUT
  fsm_inst : entity work.fsm
    port map (
      clk   => clk,
      rst   => rst,
      up    => up,
      down  => down,
      speed => speed
    );

  -- clk generation
  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
    if endsim = true then
      wait;
    end if;
  end process;

  -- Rest of stimuli
  stim_process : process
  begin
    rst    <= '1';
    wait for 4*clk_period;
    rst    <= '0';
    wait for clk_period;
    up     <= '1';
    wait for NCYCLES*clk_period;
    endsim <= true;
    wait;
  end process;


end tb_fsm_arch;
