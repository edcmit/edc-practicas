# Ejemplos de VHDL y Makefiles

[TOC]

Se proporcionan aquí algunos ejemplos de circuitos que se pueden simular y
sintetizar, para irse haciendo al manejo del lenguaje.

Se proporcionan ficheros ``Makefile`` que automatizan los comandos necesarios
para realizar los distintos pasos de simulación, síntesis e implementación. No obstante, en el desarrollo de la asignatura no necesitaremos ficheros ``Makefile``, ya que automatizaremos las simulaciones utilizando otras herramientas. En todo caso, siempre es bueno conocer los comandos que dichas herramientas ejecutan por debajo, por si fueran necesarios.

Se recomienda leer los ficheros ``Makefile``, así como los códigos VHDL, e intentar
desarrollar códigos propios.

Es muy útil pasar la opción ``-n`` a ``make`` para saber qué comandos se
ejecutarán sin necesidad de ejecutarlos (y asi no veremos la salida de los
comandos en el log), por ejemplo:

        make -n
        make view -n
        make prog -n

**IMPORTANTE:** Estos ejemplos se proporcionan como material complementario que
se recomienda que el alumno trabaje ANTES de la práctica 1. No se dedicará
ninguna sesión presencial a revisar este código, pues corresponde a material de
nivel de grado. No obstante, se atenderán dudas sobre el material en tutorías.

Si bien en la asignatura trabajaremos principalmente haciendo simulaciones, algunos de los diseños se pueden probar también en los propios dispositivos FPGA. Cuando se necesiten probar diseños en una tarjeta, se le puede solicitar una tarjeta icebreaker al profesor

## Comandos a utilizar

Se describen aquí los comandos del software, de forma que se entiendan mejor los ficheros ``Makefile``

### Simular con el simulador GHDL
Para poder simular un diseño con ghdl, es necesario realizar los siguientes pasos:

Primero se deben analizar todos los ficheros VHDL, empezando por los packages y los ficheros de menor nivel en la jerarquía. Por cada fichero VHDL, se debe ejecutar el comando:

    ghdl -a fichero.vhd

Dicho comando generará un fichero objeto, con el mismo nombre que el fichero .vhd pero de extensión .o
Una vez analizados todos los ficheros, se elabora el top-level de nuestro testbench:

    ghdl -e tb_entity

Donde tb_entity debe ser el nombre del entity de nuestro testbench. Este paso generará un fichero que, al ejecutarse, realizará la simulación.

Ejecutamos el fichero generado, indicando al simulador que genere un fichero de forma de onda en formato .ghw:

    ./tb_entity --wave=tb_entity.ghw

Si no tenemos ningún mecanismo para conseguir que la simulación pare automáticamente (por ejemplo, dejando de generar eventos de clk a partir de cierto número de ciclos), debemos utilizar la opción --stop-time=<TIME>, por ejemplo:

    ./tb_entity --wave=tb_entity.ghw --stop-time=500ns

Si durante cualquiera de los pasos se detecta algún error en el código del usuario, la herramienta indicará el fichero y la línea donde considera que tuvo origen el error.

### Visualización de formas de onda
Para visualizar las formas de onda, utilizaremos el visor de formas de onda ``gtkwave``.

Para lanzar el visor de ondas ejecutamos el siguiente comando:

    gtkwave fichero.ghw

El botón ‘reload’ de la herramienta (flecha azul circular) permite volver a cargar el fichero .ghw sin tener que cerrar y volver a abrir el programa (ni volver a configurar las formas de onda). De esta forma podemos lanzar de nuevo la simulación, en otro terminal, y cuando haya terminado simplemente pulsar el botón ‘reload’ de gtkwave.

``GTKwave`` tiene 3 áreas principales:

1. La jerarquia del diseño, arriba a la izquierda
2. Las señales y puertos disponibles en el elemento jerárquico seleccionado, abajo a la izquierda
3. El visor de formas de onda, a la derecha

Si en el área de jerarquía del diseño seleccionamos el testbench, en el área de objetos (señales y puertos) disponibles nos saldrán las señales del testbench. Podemos hacer click en el signo ``+`` a la derecha del testbench para abrir la jerarquía y ver lo que contiene (nos tiene que salir que tiene instanciado el contador). Si seleccionamos el contador podremos ver qué señales internas tiene. Podemos seleccionar varias de estas señales y, haciendo click en el botón 'Append' podemos añadirlas al visor de forma de onda. Otra forma de añadir señales es simplemente arrastrándolas hasta el área del visor de formas de onda.

![Jerarquía y señales en GTKwave](practica1/img/GTKwavemanejo1.png "Jerarquía y señales en GTKwave")

Una vez añadidas las señales a GTKwave, podemos ajustar el zoom con los botones 'Zoom Fit', 'Zoom Out' y 'Zoom In':

![Botones de zoom en GTKwave](practica1/img/GTKwavezoombuttons.png "Botones de zoom en GTKwave")

También podemos, haciendo click con el botón derecho en una señal, cambiarla de formato (hex/dec/bin/etc), de color, o insertar separadores o comentarios. También podemos seleccionar varias señales a la vez (manteniendo pulsado <kbd>Ctrl</kbd> o <kbd>Shift</kbd>) y al hacer click derecho afectaremos a todas las señales selecionadas.

En la siguiente imagen se pueden ver distintas señales en el visor de onda, algunas de ellas con diferentes formatos:

![Señales en el visor de ondas](practica1/img/GTKwavemanejo2.png "Señales en el visor de ondas")



### Síntesis con ghdl y yosys

Ya que la versión libre de ``yosys`` únicamente soporta el lenguaje verilog, utilizaremos el plugin ``ghdlsynth`` para ``yosys``

Primero tenemos que analizar con ghdl todos los ficheros fuente que vayamos a sintetizar, en orden de menor a mayor nivel en la jerarquía, por ejemplo:

    ghdl -a fichero1.vhd
    ghdl -a fichero2.vhd
    ghdl -a top.vhd

Suponiendo que la entidad descrita en ``top.vhd`` se llame ``top``, una vez que hemos analizado todos los ficheros fuente, comprobamos si existe algún error con el siguiente comando:

    ghdl --synth top

Si a la salida de este comando obtenemos una descripción VHDL, pero a bajo nivel, es que la síntesis ha sido correcta. Sin embargo, si obtenemos un mensaje de error de este tipo:

    top.vhd:18:5:error: latch infered for net "salida1"

Es que tenemos algo que corregir en nuestro código, por ejemplo en este caso sería la presencia de un latch. ``ghdl`` nos avisa si tenemos latches, pero no nos avisa si tenemos listas de sensibilidad incompletas, por lo que tendremos que tener especial cuidado con ellas, ya que nos pueden afectar a nuestras simulaciones, aunque no nos afecten a la síntesis.

Una vez que no encontremos errores con ``ghdl --synth``, realizaremos la síntesis del top_level:

    yosys -m ghdl -p 'ghdl top.vhd -e top; synth_ice40 -json top.json'

Este comando viene a significar: "utilizamos yosys con el módulo de ghdl y elaboramos el fichero top.vhd cuyo entity se llama top. Sintetizamos para FPGAs de la familia ICE40 y guardaremos la salida en el fichero top.json, que contendrá nuestro diseño sintetizado".



### Place and route con nextpnr

Para rutar el diseño sintetizado, basta con ejecutar el siguiente comando:

    nextpnr-ice40 --up5k --package sg48 --pcf top.pcf --json top.json --asc top.asc

up5k es el modelo dentro de la familia de FPGAs ice40 que vamos a utilizar. En particular, la FPGA que monta la tarjeta icebreaker tiene un encapsulado tipo sg48. Dicha familia de FPGA ha sido desarrollada por el fabricante Lattice Semiconductor. El fichero top.pcf es un fichero de asignación de pines, en el que indicamos al software a qué pines de la FPGA deben ir conectados los puertos del top-level de nuestro diseño. El fichero json es el diseño sintetizado generado en el paso anterior. El fichero .asc contendrá el diseño rutado.

Siempre que utilicemos diseños que tengan reloj, se debe añadir también el argumento --freq seguido de la frecuencia del reloj en MHz (12 MHz en la tarjeta icebreaker).

La sintaxis de las líneas del fichero .pcf (pin constraints file) es la siguiente:

    set_io nombre_puerto_top numero_pin_fpga

Por ejemplo:

    set_io boton 10
    set_io led 26

A modo de guía de localización de los pines, se puede encontrar un fichero .pcf que incluye todos los pines de la FPGA para la tarjeta que vamos a utilizar en el siguiente enlace: https://github.com/icebreaker-fpga/icebreaker-examples/blob/master/icebreaker.pcf 

### Generación del bitstream con icestorm
El último paso necesario es convertir el diseño rutado al formato de bitstream la FPGA:

    icepack top.asc top.bin

Este comando convierte el fichero con el diseño rutado top.asc en un bitstream para la FPGA ice40up5k (top.bin), con el que se puede directamente configurar la FPGA.

### Configuración de la FPGA con iceprog

Antes de poder configurar por primera vez la FPGA, debemos realizar los siguientes pasos (sólo es necesario hacerlos una vez):

1. Permitir el acceso a los dispositivos usb al usuario ``salas``:

        echo 'SUBSYSTEM=="usb", MODE="0660", GROUP="plugdev"' | sudo tee /etc/udev/rules.d/00-usb-permissions.rules
        sudo udevadm control --reload-rules

2. Añadir al usuario ``salas`` al grupo ``dialout`` para que pueda utilizar
   el terminal serie:

        sudo usermod -a -G dialout salas

   Para que se actualicen los permisos tendremos que reiniciar la sesión así que llegados a este punto podemos directamente reiniciar la máquina virtual.

   Otra opción es usar el comando ``newgrp`` permite actualizar los permisos sin tener que salir y
   volver a entrar de la sesión, pero esto sólo nos funcionará en el shell en el que ejecutemos dicho comando:

        newgrp dialout
   
Una vez realizado esto, ya podremos configurar la FPGA

Para configurar la FPGA, utilizaremos la herramienta iceprog:

    iceprog top.bin

Aunque estamos ante una FPGA de SRAM, si desconectamos y volvemos a conectar la FPGA, veremos que la configuración se almacena de manera no volátil, ya que lo que hace esta herramienta es configurar la memoria flash de la tarjeta, de la cual la FPGA leerá el bitstream.

### Automatización de los pasos

Al poderse realizar todos a través de la línea de comandos, se pueden automatizar estos pasos para un diseño concreto utilizando un Makefile o un script de consola. Aunque en la asignatura no será necesario crear Makefiles para usar estas herramientas, los propios ficheros Makefile también sirven de recordatorio de cómo se usa cada comando.



## Diseños de ejemplo

1. [Latch](latch)
   - Este código infiere un Latch, y si intentamos sintetizarlo obtendremos un
     error. ¿Podrías corregirlo?
2. [Leds](leds)
   - Este diseño simplemente mueve leds en la tarjeta en función de las
     pulsaciones de los botones
   - Lee el código y su testbench, y también el Makefile
3. Multiplexor 2 a 1
   - En [estas transparencias](https://heimdall.us.es/docs/docencia/vhdl/temas/VHDL2.pdf),
     entre las páginas 18 y 26, se pueden ver varias implementaciones posibles
     de un multiplexor 2 a 1. Entre las 5 implementaciones se muestran todas
     las sentencias que permiten tomar decisiones en un diseño VHDL.
   - Se recomienda leer y entender los códigos.
4. [Contador](contador)
   - Un contador con su testbench.
     - Lee el código del contador y su testbench y asegúrate de entenderlos
     - ¿Puedes escribir el Makefile para poder simularlo? Puedes inspirarte en
       el Makefile de los leds. Para simulación no es necesario que generes los
       ficheros ``.json``, ``.asc``, ``.bin`` ni el target ``prog``, ya que
       esos son para la implementación en la FPGA.
5. [Driver VGA](vga)
   - El driver VGA es un circuito basado en dos contadores, uno cuenta píxeles
     y otro cuenta líneas. También genera las señales de sincronismo que
     necesita el protocolo VGA y genera algunos dibujos, seleccionables
     pulsando los botones de la tarjeta.
   - En el Makefile de este diseño puedes encontrar una explicación de las
     reglas genéricas de GNU Make. Asegúrate de que entiendes la regla para
     generar ficheros ``.o`` a partir de ficheros ``.vhd``.
     - ¿Cómo afecta esta regla a la extensión del Makefile, ahora que tenemos 6
       ficheros ``.vhd``?
     - ¿Puedes escribir una regla genérica para otra de las extensiones que
       genera el Makefile, o mejorar alguna de las reglas? Por ejemplo, la
       regla que genera ``top.json`` podría beneficiarse de utilizar ``$^``
6. [FSM](fsm)
   - Este diseño es un ejemplo de cómo realizar una máquina de estados (Finite State Machine) en el lenguaje VHDL.
     - Lee el código y observa cómo tiene una estructura igual a la de las entidades vistas anteriormente: un proceso combinacional y otro proceso síncrono
     - Observa que la diferencia principal es que se define un tipo de datos específico para la máquina de estados (usando la palabra clave ``type``). El tipo de dato que se define es un tipo enumerado que únicamente puede tomar los valores indicados en la lista, lo cual es exactamente lo que necesitamos para describir máquinas de estado.
       - El sintetizador será el que se encargue de codificar el estado en señales de más o menos bits, en función del número de estados que se hayan definido
     - Observa también que, una vez definido este tipo de dato, se pueden declarar señales que tengan ese tipo de dato (típicamente ``estado`` y ``p_estado``)
     - Simula el diseño y observa con ``gtkwave`` las formas de onda, en particular la señal ``state``. Comprueba cómo sólo puede tomar los valores enumerados que hemos definido al crear el tipo de dato
     - ¿Puedes modificar el testbench para que no sólo la FSM vaya del estado ``idle`` al estado ``fast``, sino que también vuelva al estado ``idle``?
     - ¿Puedes añadir algún estado adicional a la máquina de estados?
     - ¿Puedes modificar el testbench, si fuera necesario, para que se estimule dichos estado nuevo al simular la FSM?


## Ejercicios recomendados

Se recomienda realizar los siguientes ejercicios, simulándolos a cada paso:

1. Contador
   - Escribir desde cero un contador de 8 bits sencillo, con un proceso
     combinacional que únicamente incremente el próximo valor de la cuenta y un
     proceso síncrono que incluya un reset asíncrono.
   - Añadir entrada ``enable`` al contador
     - Si ``enable`` vale '0', no se modifica el valor de la cuenta. Si vale
       '1', se incrementa en '1'.
   - Añadir un generic, ``N``, para convertirlo en un contador genérico de N
     bits.
   - Añadir un puerto de entrada ``updown``
     - Mientras el contador esté habilitado, si ``updown`` vale '1', debe
       contar hacia arriba, mientras que si vale '0', debe contar hacia abajo.
     - Si el contador está deshabilitado, no se modifica el valor de la
       cuenta, independientemente del valor de ``updown``.
   - Añadir un generic, ``SAT_VALUE``, de tipo ``integer (range 0 to 2**N-1)``,
     y una nueva salida, ``sat``, de forma que cuando el valor de cuenta sea
     igual a ``SAT_VALUE`` y el contador esté habilitado:
     - La salida ``sat`` se active combinacionalmente
     - El próximo valor de la cuenta sea 0
     - Se deja al alumno las posibles consideraciones de cómo este nuevo
       comportamiento se vería afectado por la entrada ``updown``
2. Multiplexor 4 a 1
   - Describir un multiplexor 4 a 1 de las siguientes maneras:
     1. Usando, dentro de un process, una sentencia ``if...elsif..else``
     2. Usando, dentro de un process, una sentencia ``case...when``
     3. Usando, fuera de un process, una sentencia ``when...else``
     4. Usando, fuera de un process, una sentencia ``with...select``
     5. Instanciando 3 multiplexores 2 a 1
     6. Es posible implementar este multiplexor escribiendo una única sentencia
        concurrente bastante larga. Puedes darle una pensada, te hará ver por
        qué es interesante utilizar los constructos que nos ofrece el lenguaje
        VHDL.
3. Contador y leds
   - Reutilizar el contador genérico, instanciándolo con un número suficiente
     de bits como para poder contar hasta varios millones de ciclos (la
     frecuencia del reloj externo de la FPGA es de 12 MHz)
   - Desarrollar un circuito síncrono del tipo que prefieras (por ejemplo un
     contador o un registro de desplazamiento) que se habilite con los pulsos
     de ``sat`` del contador y que, a cada activación saque una salida
     diferente por los 5 leds de la tarjeta (en PMOD2)
   - Puede simularse con un valor pequeño de ``SAT_VALUE``
   - A la hora de probarlo en una tarjeta FPGA, no olvidar usar el valor correcto de
     ``SAT_VALUE``
4. Máquina de estados
   - Cambiar el circuito anterior para que sea una máquina de estados que en
     cada estado encienda una combinación de leds diferente. Cada vez que
     la señal ``sat`` valga '1', se pasará al siguiente estado. Recordar que para hacer una máquina de estados es muy conveniente definir un tipo enumerado con la palabra reservada ``type``, como se hace en el [ejemplo de la FSM](fsm)
5. Simón dice
   - ¿Puedes hacer el juego del Simón, con una secuencia fija, utilizando los 3
     botones y 3 leds de la tarjeta?
     - Para conseguirlo, puedes adaptar la máquina de estados desarrollada
       anteriormente
     - Necesitarás también guardar en un biestable un entero con la longitud de
       la secuencia que ha acertado el jugador. Recuerda definir el rango del entero al declararlo, para evitar que se usen más bits de los necesarios
     - En la máquina de estados, a veces tendrás que avanzar en la secuencia y
       otras veces tendrás que volver al principio
