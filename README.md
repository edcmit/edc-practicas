# Electrónica Digital para Comunicaciones

Prácticas de Electrónica Digital para Comunicaciones del Máster Universitario
en Ingeniería de Telecomunicación de la Universidad de Sevilla. En este
repositorio se encontrarán únicamente las prácticas correspondientes a la parte
de diseño y verificación HDL de la asignatura.

## Contenidos

[TOC]

# Software que utilizaremos

Se plantea esta parte de la asignatura de forma que se pueda realizar
utilizando únicamente herramientas libres o de código abierto.

## Síntesis y simulación

- [Yosys](https://github.com/YosysHQ/yosys) para síntesis
- [ghdlsynth](https://github.com/ghdl/ghdl-yosys-plugin) como front-end VHDL para yosys
- [GHDL](https://github.com/ghdl/ghdl) para simulación
- [VUnit](https://vunit.github.io/) para gestión de los tests
- [gtkwave](https://github.com/gtkwave/gtkwave) para visualización de formas de onda
- [gcov](http://gcc.gnu.org/onlinedocs/gcc/Gcov.html) y [lcov](http://ltp.sourceforge.net/coverage/lcov.php) para generar informes de code coverage
- [SymbiYosys](https://github.com/YosysHQ/SymbiYosys) para verificación formal
- [Git](https://git-scm.com/) para realizar el control de versiones de nuestro código
- [Gitlab CI](https://docs.gitlab.com/ee/ci/) para integración continua

### Instalación de las herramientas:

**IMPORTANTE:** El alumno debe instalar las herramientas en su ordenador
portátil antes de la primera práctica. En caso de tener algún problema o duda,
debe escribir al profesor.

1. Descargar la máquina virtual del siguiente enlace y seguir las
   instrucciones:
   [http://trajano.us.es/~fjfj/mv/maq_virtual.html](http://trajano.us.es/~fjfj/mv/maq_virtual.html).
   No es necesario seguir las instrucciones de la sección "Fase 4. Instalación
   de software adicional"

   Esta máquina virtual es una copia de la distribución de linux existente en
   el Centro de Cálculo. Si ya la tenéis en vuestro ordenador, podéis omitir
   este primer paso.

   Se debe utilizar el usuario ``salas``, cuyo password es también ``salas``.
   Este usuario tiene permisos para hacer sudo.

2. Instalar gtkwave, gcov y lcov además de algunas dependencias de yosys y nextpnr,
   ejecutando los siguientes comandos en un terminal:

        sudo apt update
        sudo apt install gtkwave gcovr lcov gnat zlib1g-dev libcanberra-gtk-module libboost-all-dev libftdi1

3. Descargar el software para la asignatura. Se proporciona un fichero con el
   software de síntesis e implementación ya compilado:

        cd ~
        wget https://heimdall.us.es/sw/fosshdl-edc-20231010.tar.gz
        tar xzvf fosshdl-edc-20231010.tar.gz

   Se puede comprobar que la descarga ha sido correcta utilizando el siguiente
   comando:

        md5sum fosshdl-edc-20231010.tar.gz 

   La salida de dicho comando debe incluir este código (si el código es
   distinto, significa que ha habido un error en la descarga):

        0551559171df7721237fa4b72f5091be  fosshdl-edc-20231010.tar.gz

4. Instalar VUnit (y Click, una dependencia de SymbiYosys):

        sudo apt install python3-pip
        pip3 install vunit-hdl Click

5. En la consola en la que vayamos a utilizar el software, cargar las variables
   de entorno:

        source ~/fosshdl/env.rc


## Editor de texto

Si bien podemos utilizar cualquier editor de texto plano para trabajar el
código VHDL, para la asignatura utilizaremos la extensión
[TerosHDL](https://github.com/TerosTechnology/terosHDL) del editor Visual
Studio Code. Esta extensión, cuyos desarrolladores principales son antiguos
alumnos de la Escuela Técnica Superior de Ingeniería de la Universidad de
Sevilla, es software libre y ofrece muchas funcionalidades integradas para
facilitar el desarrollo con VHDL y Verilog.

### Instalación de TerosHDL

- El primer paso es instalar Visual Studio Code: https://code.visualstudio.com/Download . Se debe descargar el fichero ``.deb`` e instalarlo con el comando ``sudo dpkg -i <nombrefichero>.deb``
- El segundo paso es instalar las librerías que necesita TerosHDL:

        pip3 install teroshdl

- El tercer paso es abrir Visual Studio Code, ir a 'Extensions', buscar
  TerosHDL y hacer click en 'Install'
- Es extremadamente recomendable leer la [documentación completa de
  TerosHDL](https://terostechnology.github.io/terosHDLdoc/docs/intro)

# Planificación de las prácticas

Esta parte de la asignatura comprende las siguientes prácticas:

0. [Ejemplos de código VHDL y Makefiles](practica0/)
1. [Manejo del editor TerosHDL](practica1/)
2. [Gestión de tests con VUnit](practica2/)
3. [Creación de un testbench estructurado en VHDL](practica3/)
4. [Integración continua para diseños HDL](practica4/)
5. [Verificación formal: chequeo de modelos acotados e inducción en el tiempo](practica5/)
6. Proyecto 1
7. Proyecto 2
8. Proyecto 3
9. Presentaciones

# Control de versiones con git

Pueden encontrar una guía de manejo de git por línea de comandos en el siguiente enlace: https://gitlab.com/hgpub/gitintro

Adicionalmente, como muy tarde al terminar la práctica 1, deberán crearse una cuenta en https://gitlab.com y solicitar a acceso al grupo correspondiente a según su curso académico, según se indica en este enlace: https://gitlab.com/edcmit/edc-practicas/-/tree/main/practica1#preparaci%C3%B3n-para-la-siguiente-pr%C3%A1ctica

# Trabajo de la asignatura

El enunciado del trabajo de la asignatura puede encontrarse en la carpeta [trabajo](trabajo) de este mismo repositorio

La documentación técnica de los bloques VHDL proporcionados a los alumnos para el trabajo se publica como gitlab pages en https://edcmit.gitlab.io/edc-practicas/index.html . Pueden mirar el código de los IPs proporcionados para ver cuál es el formato de comentarios de TerosHDL (es prácticamente igual que el de Doxygen)

Si el alumno quiere generar la documentación de sus bloques (lo cual es una práctica recomendable), puede hacerlo de manera gráfica usando el botón "Module documentation preview" de TerosHDL, o en la [integración continua](https://gitlab.com/edcmit/edc-practicas/-/blob/main/faq/README.md#puedo-generar-la-documentaci%C3%B3n-t%C3%A9cnica-de-mi-c%C3%B3digo-en-la-integraci%C3%B3n-continua) una vez que haya terminado la práctica 4

# Preguntas frecuentes

Este repositorio contiene un documento de [preguntas frecuentes](faq/README.md)
el cual se irá actualizando durante el curso.

# Agradecimientos / Acknowledgement

Thank you to all developers and maintainers of the open source tools for
FPGA design and verification: Claire Wolf, Tristan Gingold, Myrtle Shah, Unai
Martínez-Corral, Matt Venn and many, many others...

Thanks also to Matt Venn for the course on formal verification you taught me
back in 2019, and Dan Gisselquist for the course material (slides and code).

Thanks to Ismael Perez Rojo, Carlos Alberto Ruiz Naranjo, Alfredo Saez and the rest of contributors to TerosHDL for this really awesome editor.

Without all your work, we would be stuck teaching with proprietary software.

Finally, thanks to [Francisco José Fernández
Jiménez](http://trajano.us.es/~fjfj) for providing and maintaining, year after
year, a Virtual Machine that mimics the operating system in ETSI's computer
rooms. Your work has saved us a lot of time all these years.
