# Preguntas frecuentes

[TOC]


## He terminado el estimador pero no me coincide el canal estimado en VHDL con el de Matlab, ¿por dónde empiezo a comprobar el diseño?

Es importante que simules todos los bloques de tu diseño por separado, ya que esto te facilitará la tarea de encontrar y corregir posibles fallos en el código.

Si ya has simulado todo por separado y el estimador completo no te funciona correctamente, puedes comprobar las siguientes cosas:

1. Que el PRBS en VHDL te da la secuencia de números aleatorios esperada. Debes comparar dicha secuencia con la generada por la implementación Matlab del PRBS y comprobar que es idéntica
2. Que estás usando el PRBS tal y como se indica el estándar: considerando que el PRBS da un signo por portadora, y no un signo por piloto. Esto significa que, para que el PRBS te dé el siguiente signo, debes habilitarlo 12 veces y no sólo una
3. Que el canal estimado en los pilotos coincide con el esperado. Si bien para ecualizar no necesitamos el canal estimado en los pilotos, es importante que comprobemos que estos valores se calculan correctamente. Si no lo hacen, cualquier interpolación entre dichos valores será necesariamente incorrecta
4. Que no estás introduciendo problemas de signo al pasar los datos de Matlab a VHDL, o al pasar la salida del VHDL a Matlab. Recuerda que, en punto fijo con signo, el bit más significativo tiene un peso negativo (``-(2^N)``)


## Estoy haciendo el ecualizador y el cociente de la división me sale siempre 0, ¿qué puede estar pasando y cómo lo arreglo?

Si el cociente te sale cero es porque tu dividendo es menor que tu divisor. En ese caso, lo mejor escalar el dividendo. Lo más sencillo es multiplicarlo por 2^M, ya que eso es simplemente un desplazamiento de bits.

Esto te producirá un cambio en el resultado, el cual ahora interpretarás con un número diferente de bits en la parte fraccional.


## ¿Puedo generar la documentación técnica de mi código en la integración continua?

Si bien TerosHDL es una extensión de VSCode, que necesita un interfaz gráfico, lo cual nos hace pensar que no se podría usar en la Integración Contínua, los creadores de TerosHDL han publicado una imagen docker con ``colibri``, la versión de línea de comandos de TerosHDL, con la que se pueden invocar a sus distintas herramientas, entre ellos el generador de documentación

Tienes un ejemplo de cómo se usa en el fichero [.gitlab-ci.yml](.gitlab-ci.yml) del repositorio, en el job ``pages``. Puedes ver cómo queda la documentación en https://edcmit.gitlab.io/edc-practicas/index.html

Los comentarios se ponen con el [formato de Doxygen](https://doxygen.nl/manual/docblocks.html), aunque por defecto el documentador de TerosHDL coge todos los comentarios y no sólo los que empiezan con el carácter ``!`` (para especificarle el carácter de inicio de los comentarios, se puede hacer pasándole la opción ``--symbol_vhdl``, por ejemplo ``--symbol_vhdl "!"``)
