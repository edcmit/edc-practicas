library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library src_lib;
--
library vunit_lib;
context vunit_lib.vunit_context;

entity tb_contador_vunit_selfcheck is
  generic (runner_cfg : string);
end;

architecture bench of tb_contador_vunit_selfcheck is


  -- Clock period
  constant clk_period : time := 5 ns;
  -- Generics

  -- Ports
  signal clk : std_ulogic;
  signal rst : std_ulogic;
  signal ena : std_ulogic;
  signal Q : unsigned(7 downto 0);

  -- Expected output
  signal expected_Q : unsigned(7 downto 0);

begin

  contador_inst : entity src_lib.contador
    port map (
      clk => clk,
      rst => rst,
      ena => ena,
      Q => Q
    );

  main : process
  begin
    test_runner_setup(runner, runner_cfg);
    while test_suite loop
      if run("test_reset") then
        info("counter: test_reset");
        -- Test sequence for test_reset
        rst <= '1';
        ena <= '0';
        wait for clk_period;
        rst <= '0';
        wait for clk_period;
        ena <= '1';
        wait for 100 * clk_period;
        rst <= '1';
        wait for clk_period;

      elsif run("test_overflow") then
        info("counter: test_overflow");
        -- Test sequence for test_overflow
        rst <= '1';
        ena <= '0';
        wait for clk_period;
        rst <= '0';
        ena <= '1';
        wait for 257 * clk_period;

      elsif run("test_alternating_ena") then
        info("counter: test_alternating_ena");
        -- Test sequence for test_alternating_ena
        rst <= '1';
        ena <= '0';
        wait for clk_period;
        rst <= '0';
        -- Let's use a for loop to avoid repeating code
        for i in 0 to 200 loop
          ena <= '1';
          wait for clk_period;
          ena <= '0';
          wait for clk_period;    
        end loop;
        
      end if;
    end loop;
    test_runner_cleanup(runner);
  end process main;

  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process clk_process;

  -- Uncomment to allow a number of errors before stopping the simulation
  -- This is useful so we have some more clk cycles in the waveform after
  -- an error is detected, which helps with debugging
  --set_stop_count(error, 2);

  -- This is not yet a Transaction-Level Model since we want to keep things
  -- simple at first, so it actually looks very similar to a circuit described
  -- in a single process
  predictor: process (rst, clk)
  begin
    if rst = '1' then
      expected_Q <= (others => '0');
    elsif rising_edge(clk) then
      if ena = '1' then
        expected_Q <= expected_Q + 1;
      end if;
    end if;
  end process;

  -- Check that circuit outputs match the expected
  -- To avoid race conditions between the UUT and the predictor, only check
  -- the values at the falling edges of clk
  checker: process (clk)
  begin
    if falling_edge(clk) then
      check(Q = expected_Q, "Q and expected_Q must be equal");
    end if;
  end process;

end;
