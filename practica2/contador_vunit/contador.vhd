library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity contador is
    port (
        clk: in std_ulogic;
        rst: in std_ulogic;
        ena: in std_ulogic;
        Q:   out unsigned(7 downto 0)  
    );
end contador;

architecture contador_arch of contador is

    signal cont, p_cont: unsigned(7 downto 0);

begin

    comb: process(ena, cont)
    begin
        if ena = '1' then
            p_cont <= cont + 1;
        else
            p_cont <= cont;
        end if;
    end process;

    sync: process(rst, clk)
    begin
        if rst = '1' then
            cont <= (others => '0');
        elsif rising_edge(clk) then
            cont <= p_cont;
        end if;
    end process;

    Q <= cont;

end contador_arch;
