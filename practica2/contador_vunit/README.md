# Ejemplo de testbench con VUnit

Creando un testbench con VUnit.

## Contenidos

[TOC]

# Ficheros

- Se proporciona un [contador de 8 bits](practica2/contador_vunit/contador.vhd) así como dos testbenches para el mismo
- El primer testbench, [``tb_contador_vunit.vhd``](practica2/contador_vunit/tb_contador_vunit.vhd) include el código necesario para que ``VUnit`` pueda gestionarlo, así como demuestra el tener distintos estímulos en función del test que se esté ejecutando
- El segundo testbench, [``tb_contador_vunit_selfcheck.vhd``](practica2/contador_vunit/tb_contador_vunit_selfcheck.vhd) es una modificación del anterior, que include el código necesario para que el testbench sea auto-chequeante
- El tercer testbench, [``tb_contador_vunit_printer.vhd``](practica2/contador_vunit/tb_contador_vunit_printer.vhd) es una modificación del primer testbench, que incluye el código necesario para exportar los datos de salida como fichero ``.csv``

Es posible combinar las funciones del segundo y tercer testbench en un único fichero.

## ``contador``

Un contador de 8 bits

## ``tb_contador_vunit``

Esta entidad contiene:

- La instancia del circuito bajo test
- Un process ``main`` en el que se inicializa el runner (con ``test_runner_setup()``), se aplican distintas secuencias de estímulos en función del test seleccionado (dentro del ``while test_suite loop``, en función de lo que devuelva la función ``run()``), y se termina limpiamente la simulación (con ``test_runner_cleanup()``)
- Un process que genera la señal de reloj ``clk``

## ``tb_contador_vunit_selfcheck``

Esta entidad es una modificación de la entidad anterior. Las diferencias son:

- Cambia el nombre el nombre de la entidad, de forma que VUnit identifique correctamente los tests si tenemos ambas entidades en el mismo proyecto
- Se define un ``signal`` que contendrá la salida esperada, ``expected_Q``
- Se añade un process ``predictor``, que calcula el valor de la salida esperada en función de las entradas. Como todavía no estamos haciendo modelado a nivel de transacción, este código se parece mucho a un circuito sintetizable (y de hecho sería sintetizable si se pusiera en una entidad aparte)
- Se añade un process ``checker``, que comprueba si la salida de la unidad bajo test coincide con la salida esperada. Para evitar condiciones de carrera, se realiza la comparación únicamente en los flancos de bajada de ``clk``

## ``tb_contador_vunit_printer``

Esta entidad es una modificación del primer testbench. Las diferencias son:

- Cambia el nombre de la entidad, nuevamente para que VUnit identifique correctamente los tests si los distintos ficheros están en el mismo proyecto
- Se declara una señal, ``running``, de tipo ``boolean`` que nos permite indicar a otros procesos de la simulación si estamos ejecutando un test o no. Esto es necesario porque el proceso que crea el fichero ``.csv`` debe almacenar datos mientras el test se está ejecutando (``running=true``) y escribir el fichero  ``.csv`` una vez que el test ha terminado (``running=false``)
- Se añade un process ``printer``, que ciclo a ciclo va almacenando la salida del circuito en un array mientras el test se está ejecutando. Una vez que el test ha terminado, escribe el fichero ``.csv``.
