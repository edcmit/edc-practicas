 library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library src_lib;
--
library vunit_lib;
context vunit_lib.vunit_context;

entity tb_contador_vunit_printer is
  generic (runner_cfg : string);
end;

architecture bench of tb_contador_vunit_printer is


  -- Clock period
  constant clk_period : time := 5 ns;
  -- Generics

  -- Ports
  signal clk : std_ulogic;
  signal rst : std_ulogic;
  signal ena : std_ulogic;
  signal Q : unsigned(7 downto 0);

  -- We need to tell the printer process when a test is running or not, so it
  -- can write the .csv file at the end of the test
  signal running : boolean := true;

begin

  contador_inst : entity src_lib.contador
    port map (
      clk => clk,
      rst => rst,
      ena => ena,
      Q => Q
    );

  main : process
  begin
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
      if run("test_reset") then
        info("counter: test_reset");
        -- Test sequence for test_reset
        rst <= '1';
        ena <= '0';
        wait for clk_period;
        rst <= '0';
        wait for clk_period;
        ena <= '1';
        wait for 100 * clk_period;
        rst <= '1';
        wait for clk_period;

      elsif run("test_overflow") then
        info("counter: test_overflow");
        -- Test sequence for test_overflow
        rst <= '1';
        ena <= '0';
        wait for clk_period;
        rst <= '0';
        ena <= '1';
        wait for 257 * clk_period;

      elsif run("test_alternating_ena") then
        info("counter: test_alternating_ena");
        -- Test sequence for test_alternating_ena
        rst <= '1';
        ena <= '0';
        wait for clk_period;
        rst <= '0';
        -- Let's use a for loop to avoid repeating code
        for i in 0 to 200 loop
          ena <= '1';
          wait for clk_period;
          ena <= '0';
          wait for clk_period;    
        end loop;
        
      end if;
    end loop;

    -- When deasserting this signal, issue a wait so the
    -- change is effective and the printer process can see
    -- the signal change (if we don't do this, we will execute
    -- the test_runner_cleanup before the printer can write the
    -- csv file)
    running <= false;
    wait for 1 ns;

    test_runner_cleanup(runner);
  end process main;

  printer: process
    -- Variable, internal to the process, where we will store the circuit
    -- outputs so they can be written to a .csv file
    -- The csv file can then be read from Matlab (using readmatrix() or
    -- csvread()) or octave (using csvread())
    variable outputs : integer_array_t;
  begin
    -- new_1d is a function defined in the VUnit libraries (specifically,
    -- in integer_array_pkg) that initializes a 1-dimensional array.
    -- There are also new_2d and new_3d functions in that package.
    outputs := new_1d;

    -- While the simulation is running, append output data to our output vector
    -- To write only one value to the new_1d object, check if ena is asserted.
    -- If this check is not performed, then the printer will write the value 
    -- multiple times depending on the test
    while (running) loop
      wait until falling_edge(clk);
      if ena = '1' then
        append(outputs, to_integer(Q));
        info("Value: " & to_string(to_integer(Q)));
      end if;
    end loop;

    -- When no more clock cycles are expected, write the file and free the
    -- memory used for the output vector
    save_csv(outputs,"counter.csv");
    deallocate(outputs);
    wait;
  end process;

  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process clk_process;

end;
