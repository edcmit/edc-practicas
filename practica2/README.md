# Práctica 2

Gestión de tests con VUnit.

## Contenidos

[TOC]

# Objetivos

El objetivo técnico de esta práctica es realizar una toma de contacto con el framework de test VUnit, de forma que podamos gestionar un número de tests creciente en un proyecto. El trabajo lo haremos integrado con el editor TerosHDL que vimos en la práctica anterior, si bien VUnit puede utilizarse también de forma independiente.

Con esto se pretenden alcanzar los siguientes objetivos formativos:

- Realizar una primera toma de contacto con el framework VUnit
- Consolidar el manejo del editor TerosHDL
- Realizar unos primeros testbenches utilizando VUnit
- Aprender cómo VUnit permite ejecutar múltiples tests con un único testbench
- Dotar al alumno de capacidades para implementar un plan de pruebas para el trabajo de la asignatura
- Alcanzar un nivel de manejo con ambas herramientas que permita desarrollar conceptos más avanzados en las siguientes prácticas

# Trabajo previo

El alumno debería haber terminado la [práctica anterior](practica1), incluyendo el diseño del PRBS y una primera verificación sencilla de los primeros datos generados por el mismo, para descartar los errores más comunes de sintaxis y de funcionamiento.

# Documentación importante

La documentación de VUnit se encuentra en: https://vunit.github.io/ . Todo lo que vamos a utilizar en esta práctica y algunas cosas que necesitarán en la siguiente se encuentran documentadas en este enlace.

# ¿Qué es VUnit?

VUnit es un entorno de test de código abierto para VHDL y SystemVerilog. Este entorno permite 'descubrir' automáticamente los test a partir del análisis del código fuente (siempre que se utilicen ciertos elementos de las librerías de VUnit), determina automáticamente el orden de compilación de los ficheros, y realiza compilación incremental, de manera que no tengamos que compilar siempre todos los ficheros de nuestro proyecto cada vez que queramos simularlo.

VUnit también incluye ciertas librerías VHDL propias y de terceros que facilitan el desarrollo de testbenches complejos, por ejemplo testbenches modelados a nivel de transacción.

Podemos encontrar una lista de las funcionalidades de VUnit en la página principal de su documentación, en https://vunit.github.io/about.html

# ¿Cómo utilizar VUnit?

Para utilizar VUnit, es necesario escribir un fichero ``run.py`` que define el proyecto de VUnit y será el punto de entrada para utilizarlo. Este fichero es python estándar, así que puede utilizar todas las funcionalidades del lenguaje (y también cualquier librería disponible en el lenguaje).

VUnit tiene 3 partes fundamentales:

1. El interfaz de línea de comandos,
2. El interfaz python, y
3. Las librerías VHDL.

## Interfaz de línea de comandos

El interfaz de línea de comandos nos permitirá pasar distintas opciones a VUnit, por ejemplo para ejecutar uno, varios o todos los tests, para generar o no las formas de onda que podemos visualizar con ``gtkwave``, o incluso generar informes de resultados de tests en formato ``xml`` para utilizar en sistemas de integración continua.

En general, el interfaz de línea de comandos lo gestionará TerosHDL, aunque ante cualquier problema o duda que podamos tener con el editor o para ejecutar nuestras simulaciones sin necesidad de abrir TerosHDL (por ejemplo al automatizarlas), la invocación más sencilla de VUnit es:

    python3 run.py

## Interfaz python

El interfaz python de VUnit se utiliza, efectivamente, desde el fichero ``run.py``, y nos permite gestionar distintos aspectos del proyecto, desde añadir ficheros al proyecto y gestionar opciones de compilación y simulación, hasta ejecutar funciones específicas antes de un test (por ejemplo, para generar las salidas esperadas utilizando otro programa), o incluso añadir argumentos personalizados al interfaz de línea de comandos.

## Librerías VHDL

Las librerías VHDL incluyen funcionalidades que podemos utilizar para crear testbenches complejos, tales como chequear valores durante la simulación, hacer 'logging' (es decir, generar mensajes de error con distintas severidades), enviar mensajes entre bloques (lo que se podría utilizar para escribir testbenches a nivel de transacción), etc.

# Primeros pasos con VUnit

## Creando un proyecto nuevo en TerosHDL

Lo primero que debemos hacer es abrir TerosHDL (habiendo cargado anteriormente las variables de entorno) y crear un proyecto nuevo. También debemos crear una carpeta en nuestra máquina virtual en la que guardaremos los ficheros de esta práctica.

## Configurar TerosHDL para que utilice VUnit

En el menú de configuración de TerosHDL, en "Tools" -> "General" -> "Select a tool, framework, simulator ..." cambiaremos ``GHDL`` por ``VUnit``, ya que ahora será VUnit quien gestionarará la compilación, invocando a ``ghdl`` según sea necesario.

## Creación del fichero run.py

A continuación, debemos crear el fichero ``run.py``. Utilizaremos el fichero que se proporciona de ejemplo en la documentación de VUnit, simplemente se ha cambiado el nombre de la librería en la que se compilarán nuestros ficheros para que coincida con la que aparece en las plantillas de testbench VUnit que genera TerosHDL (``src_lib``):

```python
from vunit import VUnit

# Create VUnit instance by parsing command-line arguments
vu = VUnit.from_argv()

# Create library 'src_lib', where our files will be compiled
lib = vu.add_library("src_lib")

# Add all files ending in .vhd in current working directory to our library 'src_lib'
lib.add_source_files("*.vhd")

# Run vunit function
vu.main()
```

Podemos ver que este script python realiza únicamente 5 acciones:

1. Importar la librería python de VUnit
2. Crear una instancia de VUnit a partir de los argumentos recibidos por la línea de comandos
3. Crear una librería en la que se compilará todo nuestro código
4. Añadir todos los ficheros con extensión ``.vhd`` que están en la misma carpeta que ``run.py`` a dicho library
5. Ejecutar la función principal de VUnit, que realizará unas acciones u otras (ejecutar uno o varios tests, generar ficheros de formas de onda, etc) en función de los argumentos recibidos por línea de comandos

Si bien ahora mismo no será necesario hacer cambios en este fichero python, el alumno es libre de modificarlo de acuerdo a sus necesidades, siempre teniendo en cuenta la documentación del interfaz python de VUnit, disponible en: https://vunit.github.io/py/ui.html

## Creación de un primer testbench VUnit

Un testbench VUnit es exactamente igual que un testbench VHDL estándar, sólo que:

1. Incluye la librería ``vunit_lib`` y el contexto ``vunit_context`` de dicha librería (un ``context``, en VHDL-2008, es una declaración de un conjunto de llamadas a ``library`` y ``use`` en una única unidad que puede ser reutilizada, lo que nos evita tener secciones library grandes cuando estamos siempre reutilizando los mismos libraries y packages)
2. La entidad tiene un ``generic`` llamado ``runner_cfg``, de tipo ``string``, por la que se recibirá la configuración del 'test runner', es decir del ejecutor del test
3. El proceso principal del testbench comienza con una llamada al procedimiento ``test_runner_setup(runner, runner_cfg)``, y termina con una llamada al procedimiento ``test_runner_cleanup(runner)``
4. El testbench no incluye ningún otro mecanismo para parar la simulación, tales como assertions con severidad 'failure', llamadas a ``std.env.finish``, asfixia de reloj u otros. Los testbenches VUnit siempre deben ser terminados por la llamada a ``test_runner_cleanup``.

Ahora añadimos un testbench al proyecto con el siguiente contenido, también proporcionado como ejemplo en la documentación de VUnit:

```vhdl
library vunit_lib;
context vunit_lib.vunit_context;

entity tb_example is
  generic (runner_cfg : string);
end entity;

architecture tb of tb_example is
begin
  main : process
  begin
    test_runner_setup(runner, runner_cfg);
    report "Hello world!";
    test_runner_cleanup(runner); -- Simulation ends here
  end process;
end architecture;
```

Al guardar el fichero (si lo nombramos igual que la entidad, se llamará ``tb_example.vhd``), nos debería aparecer el test en TerosHDL, en 'Runs'. Si no nos aparece, pulsaremos el icono 'Refresh' en 'Runs' (la flecha circular).

Una vez detectado correctamente el test, que deberia llamarse ``src_lib.tb_example.all``, podremos ejecutarlo igual que ejecutábamos los tests en la [práctica anterior](practica1). Hay que comentar aquí que, por cómo funciona VUnit, si hemos configurado TerosHDL en modo GUI, abrirá ``gtkwave`` automáticamente tras cada test. En este caso, como no tenemos ninguna señal moviéndose en todo el testbench, no se abrirá ``gtkwave``, ya que no hay ninguna forma de onda que visualizar, pero esto cambiará en cuanto tengamos señales que cambien de valor en el testbench.

_Nota: si queremos ejecutar múltiples tests utilizando el botón 'Run All', puede ser conveniente ir a la configuración de Teros y en "Tools" -> "General" seleccionar temporalmente el modo de ejecución "Command line", para no tener que andar cerrando ventanas de GTKwave a mano_

Si bien para poder ejecutar los tests sólo es necesario añadir el fichero ``run.py`` al proyecto, para poder editar los ficheros VHDL más fácilmente podemos añadirlos también al proyecto. En todo caso, el top-level del proyecto debe ser el fichero ``run.py``, ya que es el punto de entrada para poder usar VUnit.

## Eliminar los warnings del linter en TerosHDL

Habrán visto que, en el testbench, TerosHDL les marca algunos errores tales como 'No such library 'src_lib'' o 'No such library 'vunit_lib''. Para arreglar esto, tenemos que añadir dichos libraries al proyecto.

Para crear el library ``src_lib``, utilizaremos el botón 'Add library' dentro del proyecto, asignándole el nombre correo (``src_lib``), e incluyendo los ficheros que la componen. Esto significa que debemos eliminar nuestro testbench del proyecto (con el botón del signo ``-``, 'Delete', que borra el fichero del proyecto pero no del disco duro) y volver a añadirlo al proyecto, esta vez dentro de la librería (usando el botón del signo ``+``, 'Add file', dentro de la librería).

Para la librería ``vunit_lib`` es un poco más difícil localizar los ficheros, ya que por defecto están en una carpeta oculta, y adicionalmente ``vunit_lib`` referencia a su vez otras librerías. Para facilitarles este proceso, les he dejado un [fichero ``.csv``](practica2/vunit_lib.csv) que pueden descargar. Una vez que lo hayan descargado, deben hacer click en el botón 'Add file' del proyecto (es decir, sin darle al botón de crear nueva librería), seleccionar 'Load files from file list (CSV)', y seleccionar el fichero ``vunit.csv``, lo que creará el library nuevo con los ficheros correctos.

Una vez realizado esto, deben desaparecer todos los problemas al respecto de la localización de las librerías.

## Múltiples tests en el mismo testbench

Cuando tenemos testbenches complejos, normalmente queremos aprovechar la estructura que hemos creado en VHDL para realizar múltiples tests. Por ejemplo, si tenemos un tesbench modelado a nivel de transacción con múltiples interfaces, podemos querer tener un conjunto de tests que cada uno pruebe un interfaz distinto, y un test final que estimule todos los interfaces simultáneamente. También nos puede interesar en testbenches más sencillos, donde tal vez querramos hacer un test dirigido y otro aleatorio, o incluso distintos tests estimulando con un número distinto de datos o utilizando la entidad bajo test en distintas configuraciones.

Para poder disponer de múltiples tests en el mismo testbench, tenemos que añadir un ``while test_suite loop`` al process principal de nuestro testbench, como puede verse en el siguiente ejemplo de la documentación de VUnit:

```vhdl
library vunit_lib;
context vunit_lib.vunit_context;

entity tb_example_many is
  generic (runner_cfg : string);
end entity;

architecture tb of tb_example_many is
begin
  main : process
  begin
    test_runner_setup(runner, runner_cfg);

    while test_suite loop

      if run("test_pass") then
        report "This will pass";

      elsif run("test_fail") then
        assert false report "It fails";

      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;
end architecture;
```

De esta forma, VUnit descubrirá los tests ``test_pass`` y ``test_fail`` automáticamente. Si creamos este fichero en la carpeta donde tenemos el ``run.py``, lo guardamos, y pulsamos el botón 'Refresh' en 'Runs List', deberían descubrirse dos nuevos tests: ``src_lib.tb_example_many.test_pass`` y ``src_lib.tb_example_many.test_fail``. De esta forma, los tests se nombran siguiendo el esquema ``nombre_libreria.nombre_entidad.nombre_test``. La función ``run`` es proporcionada por el library ``vunit_lib``, al igual que los procedimientos ``test_runner_setup`` y ``test_runner_cleanup``. Dentro de cada ``if run("testname") then`` escribiremos el código específico de cada test.

Se pueden ejecutar todos los tests del proyecto haciendo click en el botón 'Run All' en la sección 'Runs'.

# Trabajo a realizar

En esta práctica deben adaptar el testbench que han hecho para el PRBS a VUnit. Para ello, lo primero será realizar los cambios indicados en [Creación de un primer testbench VUnit](#creación-de-un-primer-testbench-vunit). En particular, no se olviden de eliminar el mecanismo de asfixia de reloj, ya que si un testbench VUnit termina, por cualquier razón, antes de llegar al procedimiento ``test_runner_cleanup``, se marcará ese test como fallido.

También pueden usar el generador de plantillas de TerosHDL, seleccionando 'VUnit testbench' para que les genere el código del testbench VUnit del PRBS, y viendo el código generado, combinar dicho código con el que desarrollaron en la práctica anterior. Esta funcionalidad les será útil para crear los testbenches de los siguientes bloques con los que trabajen.

Para poder saber qué funciones (y procedimientos) VHDL de VUnit pueden utilizar para implementar las funcionalidades necesarias para el testbench, tales como: chequeo de valores, logging de errores y/o valores correctos, señalizar a VUnit que el test ha fallado cuando la salida no coincide con la esperada, etc, deben mirar la [documentación de las librerías  VHDL de VUnit](https://vunit.github.io/vhdl_libraries.html). Pueden centrarse en las librerías de 'logging' y 'check', que serán suficientes para realizar esta práctica. Verán en la documentación que a las funciones de 'check' pueden indicarle la severidad de las discrepancias encontradas (tales como warning, error o failure), y que por defecto un test se marca como fallido si se detectan uno o más errores.

A la hora de usar las librerías de logging, se recuerda que VHDL-2008 define una función ``to_string()`` para casi todos los tipos de dato, de esta forma se pueden imprimir los valores que toman los distintos objetos en el log de la consola cuando sea necesario, por ejemplo:

    debug("tb_counter: el valor de la cuenta es: " & to_string(cuenta));

El operador concatenación (``&``) concatena en este caso dos ``string``, de forma que la función ``debug()`` recibe una única cadena de caracteres

También necesitarán predecir de alguna manera la salida esperada del PRBS: esto NO deben hacerlo exactamente de la misma manera que tengan implementado el VHDL sintetizable (ya que en ese caso, no estarían verificando nada). Entonces, la recomendación es que no calculen esos valores utilizando VHDL sintetizable escrito en dos procesos, sino que lo hagan por ejemplo en un único process y posiblemente utilizando otros tipos de datos diferentes a los que usaron antes. Idealmente deben copiar la estructura del código que han realizado en Matlab para el PRBS e implementar eso en el predictor VHDL del testbench, aunque tendrán que tener en cuenta la entrada de habilitación del PRBS en VHDL, así como las entradas de reset y reloj.

Se proporciona un ejemplo de testbench auto-chequeante, para un contador, en [practica2/contador_vunit](practica2/contador_vunit)

# Trabajo no presencial

## Lectura de la documentación de VUnit

Deben realizar una primera lectura jerárquica de la documentación de VUnit, en https://vunit.github.io . Ya que esta documentación es más extensa, se recomienda centrarse en las secciones "Python Interface" y "VHDL Libraries". Por supuesto la sección "Examples" también contiene contenido que les puede ser interesante. Por _lectura jerárquica_ me refiero a que primero lean los índices, localizando dónde se documentan las distintas funcionalidades y tomando nota mental de qué cosas pueden ser interesantes, y luego en una segunda pasada lean en profundidad aquellas secciones que les hayan llamado la atención.

Ya que en la siguiente práctica realizaremos un testbench estructurado (es decir, modelado a nivel de transacción) para el interpolador lineal del proyecto, deben asegurarse de haber localizado las funcionalidades de las librerías VHDL de VUnit que les permitan implementar dichas funcionalidades, tales como generación de transacciones (creando y enviando mensajes, en el 'Communication Library'), comprobación de valores (en el 'Check Library'), y generación de mensajes de log con distintos niveles de severidad ('Logging Library').

Ya que VUnit es software libre, pueden encontrar todo su código fuente en https://github.com/VUnit/vunit . En particular, pueden encontrar las definiciones de funciones y procedimientos en el código fuente, y pueden buscar en dicho código, por ejemplo usando TerosHDL.

Sin embargo, si lo prefieren, también pueden buscar en el código utilizando herramientas de shell como ``grep``, por ejemplo para encontrar todas las veces que aparece ``test_runner_setup`` en el repositorio, pueden hacer, en la consola de comandos:

    cd vunit
    grep -r test_runner_setup *

Para ayudarles a entender comandos de shell que no comprendan, un recurso útil es https://explainshell.com/ , donde pueden pegar un comando de consola y la web les indicará qué hace cada elemento del comando.

## Comprobación contra Matlab u Octave del PRBS implementado en VHDL

Para realizar la comprobación del PRBS implementado en VHDL contra su implementación en Matlab tienen varias opciones:

  - Utilizar funciones del 'Logging Library' de VUnit para escribir un fichero con las salidas del PRBS VHDL, leer este fichero desde Matlab y comprobar desde dentro de Matlab que ambas secuencias coinciden. Se debe probar al menos un periodo completo del PRBS, hasta que se vuelva al valor inicial, no obstante es buena idea comprobar más ciclos (por ejemplo, comprobando varios periodos completos del PRBS)
  - Incluir en el fichero ``run.py`` una llamada a un script ``octave`` (una alternativa libre a Matlab, disponible en la máquna virtual a través del gestor de paquetes ``apt``) que genere los valores esperados antes de realizar la simulación, y desde la simulación del PRBS comparar la salida del VHDL con los datos del fichero generado. Esto tiene dos ventajas: la primera, que es posible indicar exactamente en qué ciclo de reloj la salida no es correcta, facilitando los esfuerzos de depuración, y la segunda, que no dependemos de una herramienta propietaria. En particular, esta segunda ventaja nos facilitará el trabajo cuando tengamos que ejecutar los tests en un sistema de integración continua.
  - También pueden generar el fichero de salidas del VHDL y comparar contra el modelo Matlab usando ``octave`` una vez que la simulación ha terminado. VUnit proporciona funcionalidad, en su interfaz python, para ejecutar funciones arbitrarias tanto antes como después de ejecutar la simulación del VHDL.

  Este nuevo test (o tests) se puede añadir como un test adicional al mismo testbench (como se describe en la sección [Múltiples tests en el mismo testbench](#múltiples-tests-en-el-mismo-testbench)), de forma que mantengan el test anterior (cuyo predictor está realizado en VHDL) y puedan ejecutar ambos, incrementando así la confianza en el diseño (y en su modelo Matlab).

Se proporciona un ejemplo de testbench que escribe la salida del circuito en formato ``.csv``, para un contador, en [practica2/contador_vunit](practica2/contador_vunit)

## Guardar copia de su trabajo

Se deben añadir, al repositorio ``git`` que se les ha creado (o se les creará en breve) para el trabajo de la asignatura, todos los códigos desarrollados. Se recuerda que tienen disponible una guía de ``git`` en el siguiente enlace: https://gitlab.com/hgpub/gitintro


