# Práctica 3

Creación de un testbench estructurado.

## Contenidos

[TOC]

# Objetivos

El objetivo técnico de esta práctica es modificar el testbench de la práctica anterior para implementar modelado a nivel de transacción, permitiendo realizar testbenches estructurados como se ha visto en las clases de teoría.

También se modificará el fichero `run.py` para que genere automáticamente informes de _code coverage_

Con esto se pretenden conseguir los siguientes objetivos formativos:

- Consolidar los conocimientos más abstractos de verificación funcional vistos en teoría mediante su aplicación práctica
- Consolidar el manejo de la funcionalidad básica de VUnit vista en la práctica anterior
- Comprender el manejo a nivel de usuario de las funciones de paso de mensajes de la 'Communication Library' de VUnit
- Comprender la generación de estímulos aleatorios utilizando las funcionalidades de OSVVM (Open Source VHDL Verification Methodology)
- Implementar unas métricas de alcance funcional (_functional coverage_) utilizando las funcionalidades de OSVVM
- Dotar al alumno de herramientas que pueda utilizar en el trabajo de la asignatura
- Dotar al alumno de herramientas que pueda utilizar para verificar cualquier clase de diseño HDL


Si bien los testbenches a nivel de transacción de se pueden realizar de múltiples formas y utilizando distintas librerías, en este caso nosotros vamos a utilizar las librerías de comunicación de VUnit que se mencionaron en la práctica anterior.

# Trabajo previo

El alumno debería haber terminado la práctica anterior y haber leído la documentación de VUnit que se le indicó.

# Documentación importante

Hay tres documentos importantes de cara a la realización de esta práctica:

## Documentación de la librería de comunicaciones de VUnit

- La documentación de la 'com' library de VUnit se encuentra en https://vunit.github.io/com/user_guide.html

Esta librería de VUnit nos permitirá que los diferentes elementos de nuestro testbench se comuniquen entre sí enviando transacciones

## Documentación del package RandomPkg de OSVVM

- Referencia rápida: https://github.com/OSVVM/Documentation/blob/master/RandomPkg_quickref.pdf
- Guía de usuario: https://github.com/OSVVM/Documentation/blob/master/RandomPkg_user_guide.pdf

Este package nos permitirá generar estímulos aleatorios para nuestros diseños

## Documentación del package CoveragePkg de OSVVM

Este package nos permitirá tomar métricas de alcance funcional sobre nuestros diseños, monitorizando las transacciones de entrada y salida de nuestros diseños y anotando a qué 'bins' pertenece cada transacción

- Referencia rápida: https://github.com/OSVVM/Documentation/blob/master/CoveragePkg_quickref.pdf
- Guía de usuario: https://github.com/OSVVM/Documentation/blob/master/CoveragePkg_user_guide.pdf

# Modificando `run.py`

Realizaremos los siguientes cambios en nuestro fichero `run.py`:

1. Permitirmos usar la función `call()` para lanzar ejecutables externos desde el código python:

```python
# Allow the use of call()
from subprocess import call
```

2. Habilitaremos la librería de comunicación de VUnit y OSVVM. De esta forma, VUnit las compilará automáticamente y sus funcionalidades estarán disponibles para ser utilizadas desde nuestros testbenches:

```python
# Enable the communication library ('com')
vu.add_com()

# Enable OSVVM (Open Source VHDL Verification Methology)
vu.add_osvvm()
```

3. Indicaremos a VUnit que queremos habilitar el _code coverage_ en el simulador (para nuestra librería 'src_lib'). VUnit se encargará de invocar a GHDL con las opciones necesarias para que esto ocurra:

```python
# Enable code coverage collection
lib.set_sim_option("enable_coverage", True)
lib.set_compile_option("enable_coverage", True)
```

4. Definiremos una función `post_run` (es un 'hook' de VUnit) que, tras ejecutar los tests, fusionará los resultados de code overage de todos los tests en una única base de datos (usando `gcovr`), generará un informe (usando `lcov`) y convertirá dicho informe a formato html (usando ``genhtml``):

```python
# Use the post_run hook to tell VUnit that it has to merge coverage data from
# all tests (with gcovr) and create the html reports (with lcov and genhtml)
def post_run(results):
    results.merge_coverage(file_name="coverage_data")
    if vu.get_simulator_name() == "ghdl":
        call(["gcovr", "coverage_data"])
        call(["lcov", '--capture', '--directory', '.', '--output', 'coverage.info'])
        call(["genhtml", "coverage.info", "--output-directory", "coverage_report"])
```

5. Al invocar a VUnit, tenemos que indicarle que, tras ejecutar los tests (fase `post_run`) invoque a la función `post_run` que hemos definido:

```python
# Run vunit function, but pass to it2 the post_run hook
vu.main(post_run=post_run)
```

Se proporciona el script completo en [run.py](practica3/run.py)

# Añadiendo las nuevas librerías al proyecto en TerosHDL

Se proporciona un fichero ``.csv`` con la localización de los ficheros de las librerías ``vunit_lib`` (incluyendo la 'communication library') y ``osvvm`` en [practica2/vunit_lib.csv](practica2/vunit_lib.csv)

# Creando un testbench estructurado

Se proporciona un [ejemplo de testbench estructurado para un contador](practica3/tb_contador_vunit_selfcheck_tlm.vhd), continuando con el ejemplo que se proporcionó para la práctica anterior.

## Planteamiento

### Modelado a nivel de transacción

Para implementar el modelado a nivel de transacción lo que haremos será:

1. Definir tipos de datos específicos para cada transacción
2. Crear receptores de mensajes de VUnit para quienes deban recibir transacciones
3. Quienes deban enviar transacciones, deberán:
   1. Meter las transacciones en mensajes (usando `push`)
   2. Enviar (`send`) dichos mensajes a la red de mensajes de VUnit (`net`), indicando para qué receptor está destinado el mensaje
4. Quienes deban recibir transacciones, deben:
   1. Usar `receive` para recibir el mensaje
   2. Usar `pop` para sacar la transacción del mensaje

### Depuración usando ``to_string``

Para ver en los logs de las simulaciones el contenido de las transacciones, haremos uso de la función ``to_string`` de VHDL-2008

## Elementos del testbench

A continuación se explican los distintos elementos del testbench:

### Sección library

En la sección library simplemente tenemos que habilitar la librería de comunicaciones de VUnit y OSVVM, junto con los paquetes de la misma que vamos a utilizar (``randompkg`` y ``coveragepkg``):

```vhdl
-- Use VUnit, load its main context and the communication library
library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;

-- Use OSVVM for random testing and functional coverage
library osvvm;
use osvvm.randompkg.all;
use osvvm.coveragepkg.all;
```

### Sección architecture, antes del `begin`

#### Transacciones de entrada y salida

En la sección architecture podemos describir las transacciones que vamos a utilizar. Tiene mucho sentido definir las transacciones utilizando datos de tipo `record`:

```vhdl
  type input_tran_t is record
    reset   : boolean;
    enable  : boolean;
    cycles  : integer;
  end record;

  type output_tran_t is record
    value : integer;  
  end record;
```

También, para cada transacción, crearemos:

- Un procedure ``push`` (para empujar una transacción a un mensaje de VUnit)
- Una función ``pop`` (para sacar una transacción de un mensaje de VUnit)
- Una función ``to_string`` (para ayudarnos a la depuración)

Por ejemplo para las transacciones de entrada:

Procedure ``push``:

```vhdl
  -- Since VUnit's communication library only allows to push and pop simple
  -- datatypes, we'll define our own procedures to push and pop complete
  -- transactions, using the already-defined push and pop procedures
  -- We will also define our own to_string() functions for debugging purposes
  procedure push(msg: msg_t; input_tran: input_tran_t) is
  begin
    push(msg, input_tran.reset);
    push(msg, input_tran.enable);
    push(msg, input_tran.cycles);
  end procedure;
```

Función ``pop``:

```vhdl
  -- Impure functions can read and write out of their scope, they have
  -- 'side effects'. Our 'pop' must be impure since it calls VUnit's 'pop'
  -- functions, which are impure themselves
  impure function pop(msg: msg_t) return input_tran_t is
    variable input_tran : input_tran_t;
  begin
    input_tran.reset  := pop(msg);
    input_tran.enable := pop(msg);
    input_tran.cycles := pop(msg);
    return input_tran;
  end function;
```

Función ``to_string``:

``` vhdl
  function to_string(input_tran: input_tran_t) return string is
  begin
    return "[input_tran] (reset => " & to_string(input_tran.reset) & ", enable => " & to_string(input_tran.enable) & ", cycles => " & to_string(input_tran.cycles) & ")"; 
  end function;
```

Todo este código se puede mover a su propio package: en un proyecto más complejo tendríamos un package para describir las transacciones de cada interfaz, y lo importaríamos desde aquellos testbenches que lo necesiten.

#### Receptores de mensajes

Se debe declarar un receptor de mensajes de VUnit para cada elemento del testbench que pueda recibir transacciones. En el caso del `checker`, declararemos dos receptores, uno para las transacciones que vienen del DUT y otro para las que vienen del `predictor`:

```vhdl
  constant driver_receiver : actor_t := new_actor("driver receiver");
  constant predictor_receiver : actor_t := new_actor("predictor receiver");
  constant checker_receiver_actual : actor_t := new_actor("checker receiver actual");
  constant checker_receiver_expected : actor_t := new_actor("checker receiver expected");
```

En este testbench todos los elementos que deben comunicarse están en la misma arquitectura, pero en proyectos más complejos esto no tiene por qué ser así. En ese caso, el productor de transacciones tendrá que usar la función `find` para encontrar el receptor al que quiera enviar transacciones, según se indica en la documentación de la librería, o bien se pueden pasar los receptores de mensajes (tipo de dato ``actor_t``) como generics de los distintos bloques.

### Sección architecture, tras el `begin`

#### Secuencia de test (process `main`)

Este process va a generar las transacciones de entrada, que irán tanto al driver como al predictor.

El process tiene definido un `procedure` antes de su `begin`, para agrupar las distintas acciones que tenemos que aplicar a cada transacción que generemos:

```vhdl
    -- For each transaction we have to do multiple things, so let's encapsulate:
    --  0. Populate input_tran with the correct fields (input for this procedure)
    --  1. Print the transaction values for debugging
    --  2. Create a new message
    --  3. Push the transaction to the message
    --  4. Make a copy of the message so we can send a copy to the predictor
    --     (the message gets deleted when sent)
    --  5. Send the message with the transaction to the driver
    --  6. Send the copy of the message to the predictor
    --  7. Wait a bit of time so the simulation can run (if not, we would
    --     reach the cleanup function before the circuit could react)
    procedure seq_tran(constant input_tran: in input_tran_t) is
      variable msg : msg_t;
      variable msg_copy : msg_t;
    begin
      info("test_sequence: pushing:" & to_string(input_tran));
      msg := new_msg;
      push(msg, input_tran);
      msg_copy := copy(msg);
      send(net, driver_receiver, msg);
      send(net, predictor_receiver, msg_copy);
      wait for input_tran.cycles*clk_period;
    end procedure;
```

De esta forma, usando `seq_tran` podremos crear secuencias de estímulos complejas utilizando una única línea de código por cada transacción. Por ejemplo, el primer test queda de la siguiente manera:

```vhdl
      if run("test_reset") then
        info("counter: test_reset");
        -- Test sequence for test_reset
        seq_tran((reset => true, enable => false, cycles => 1));
        seq_tran((reset => false, enable => true, cycles => 100));
```

#### Driver

El driver convierte transacciones a movimiento de pines. Para poder recibir las transacciones necesitará una variable tipo `msg_t` en la que almacenar los mensajes recibidos. El procedure `receive` es bloqueante, es decir, que el driver no saldrá de él hasta que haya recibido una transacción, por lo también nos sirve para esperar a que tengamos una transacción. Una vez que se extrae la transacción del mensaje, el movimiento de pines es tan sencillo como hacer asignaciones a los pines del DUT y esperar los tiempos necesarios:

```vhdl
  -- Convert input transactions to pin movement
  driver: process
    variable msg: msg_t;
    variable input_tran: input_tran_t;
  begin
    info("driver: waiting for transaction...");
    -- Wait until a transaction is received
    receive(net, driver_receiver, msg);
    -- Extract the transaction from the message
    input_tran := pop(msg);
    info("driver: received:" & to_string(input_tran));
    -- Drive the UUT pins
    if input_tran.reset then
      rst <= '1';
    else
      rst <= '0';
    end if;
    if input_tran.enable then
      ena <= '1';
    else
      ena <= '0';
    end if;
    for i in 0 to input_tran.cycles loop
      wait for clk_period;
    end loop;
  end process;
```

#### Monitor

El monitor convierte movimiento de pines a transacciones. Por simplicidad, se ha optado por enviar una transacción de salida por cada ciclo de reloj, aunque existen otras formas de enfocar esto que resultarían en menos transacciones. El monitor necesitará una variable de tipo `msg_t` a la que 'pushear' la transacción de salida obtenida. Una vez formado el mensaje simplemente se envía al checker utilizando `send`. Se recuerda que al utilizar `send` se elimina el mensaje enviado, por eso siempre hay que inicializarlo de nuevo haciendo `msg := new_msg;`:

```vhdl
  -- Convert pin movement to output transactions, which is somewhat
  -- direct for the counter: we have an output value each clock cycle
  monitor: process
    variable msg: msg_t;
    variable output_tran: output_tran_t;
  begin
    wait until falling_edge(clk);
    output_tran.value := to_integer(Q);
    info("monitor: detected:" & to_string(output_tran));
    msg := new_msg;
    push(msg, output_tran);
    send(net, checker_receiver_actual, msg);
  end process;
```

#### Predictor

El predictor predice las transacciones de salida en función de las transacciones de entrada recibidas. En este caso, por cada transacción de entrada podrá enviar múltiples transacciones de salida. El predictor es a la vez un productor y un consumidor de transacciones, por lo que debe tanto recibir mensajes y extraer transacciones de ellos como crear mensajes con transacciones y enviarlos a la red.

```vhdl
  -- Predict the expected output, now at transaction level
  -- Due to the nature of the counter, and to simplify these transaction-level
  -- models, each input transaction will generate a number of output
  -- transactions (exactly input_tran.cycles transactions)
  -- A more elegant solution, with less output transactions can be implemented,
  -- but it would make both the monitor, predictor and checker more complex
  predictor: process
    variable input_tran: input_tran_t;
    variable expected_output_tran: output_tran_t;
    variable expected_Q: integer;
    variable msg: msg_t;
  begin
    expected_Q := to_integer(Q);
    -- Do everything in an infinite loop because we don't want to exit the
    -- process, since we want to keep the value of the internal variable
    -- expected_Q
    while true loop
      -- Wait until a transaction is received
      receive(net, predictor_receiver, msg);
      -- Extract the transaction from the message
      input_tran := pop(msg);
      info("predictor: input:" & to_string(input_tran));

      -- Send as many output transactions as cycles in the input_tran
      for i in 0 to input_tran.cycles loop
        if input_tran.reset then
          expected_Q := 0;
        elsif input_tran.enable then
          expected_Q := (expected_Q + 1) mod 256;
        end if;
        -- Create the output transaction and send it to the checker
        expected_output_tran.value := expected_Q;
        info("predictor: expected:" & to_string(expected_output_tran));
        msg := new_msg;
        push(msg, expected_output_tran);
        send(net, checker_receiver_expected, msg);
      end loop;
    end loop;
  end process;
```

#### Checker

El checker debe comparar las transacciones de salida que vienen del diseño bajo test con las transacciones de salida esperadas generadas por el predictor. El caso más sencillo de un checker es aquel en el que lo único que tenemos que hacer es comprobar que ambas transacciones sean iguales, como ocurre en este caso:

```vhdl
  -- Check that circuit outputs match the expected
  checker: process
    variable msg_actual: msg_t;
    variable msg_expected: msg_t;
    variable output_tran_actual: output_tran_t;
    variable output_tran_expected: output_tran_t;
  begin
    -- Wait for both actual and expected output transactions
    receive(net, checker_receiver_actual, msg_actual);
    receive(net, checker_receiver_expected, msg_expected);
    -- Extract the transactions from the message
    output_tran_actual := pop(msg_actual);
    output_tran_expected := pop(msg_expected);
    info("checker: actual:" & to_string(output_tran_actual));
    info("checker: expected:" & to_string(output_tran_expected));
    -- Check transactions match
    check(output_tran_actual.value = output_tran_expected.value, "checker: actual value (" & to_string(output_tran_actual.value) & ") and expected value (" & to_string(output_tran_expected.value) & ") must be equal");
  end process;
```

### Test aleatorio

Para conseguir hacer test con entradas aleatorias (_random testing_) necesitaremos declarar un generador de números aleatorios de OSVVM en el process en el que tenemos la secuencia de test:

```vhdl
  main : process

    variable randomgen : RandomPType;
```

Y una vez que tenemos esta variable declarada sólo tenemos que inicializar la semilla del generador una vez, e invocar a una función del generador cada vez que queramos generar un valor aleatorio (en función del tipo de dato que queramos generar, llamaremos a una función u otra):

```vhdl
      if run("test_random") then
        info("counter: test_random");
        -- Test sequence for test_random

        -- Initialize random seed
        randomgen.InitSeed(randomgen'instance_name);

        for i in 0 to 20 loop
          seq_tran((reset => randomgen.RandBool, enable => randomgen.RandBool, cycles => randomgen.RandInt(0,511)));
        end loop;

      end if;
    end loop;
```

Como pueden ver, `RandBool` genera valores de tipo `boolean` mientras que `RandInt` genera valores de tipo `integer`. En la documentación del package pueden ver todas las funciones disponibles.

### Functional coverage

El testbench proporcionado no incluye alcance funcional (_functional coverage_), pero se dejan aquí los pasos a seguir para añadirlo:

1. Deberemos crear dos nuevos receptores de mensajes, antes del `begin` de nuestra arquitectura:

```vhdl
  constant coverage_receiver_input : actor_t := new_actor("coverage receiver input");
  constant coverage_receiver_output : actor_t := new_actor("coverage receiver output");
```

2. También antes del `begin` de la arquitectura, tendremos que crear los objetos en los que se capturará el functional coverage:

```vhdl
  -- Objects for functional coverage
  signal cover_reset: CoverageIDtype := NewID("cover_reset");
  signal cover_enable: CoverageIDtype := NewID("cover_ena");
  signal cover_cycles: CoverageIDtype := NewID("cover_cycles");
  signal cover_value: CoverageIDtype := NewID("cover_value");
```

3. Una vez hecho esto, tendremos que modificar tanto la secuencia de test (process main) como el monitor de protocolo para que manden copias de las transacciones de entrada y salida a los receptores `coverage_receiver_input` y `coverage_receiver_output` respectivamente. Se debe copiar el mensaje una vez que ya ha sido 'pusheada' la transacción al mismo, pero antes de enviar dicho mensaje a la red.

4. Crearemos un proceso para medir el alcance funcional en las transacciones de entrada:

```vhdl
  -- Collect functional coverage on input transactions
  -- GenBin creates bins for the coverage object:
  --   GenBin(min,max) creates one bin per value between min and max (both included)
  --   GenBin(min,max,nbins) creates nbins bins, dividing the range between min and max evenly
  -- The bool2int function is necessary because the bins always have integer values or ranges
  coverage_input: process
    variable msg: msg_t;
    variable input_tran: input_tran_t;

    function bool2int (bool: boolean) return integer is
      variable retval : integer;
    begin
      if bool then
        retval := 1;
      else
        retval := 0;
      end if;
      return retval;
    end;

  begin
    AddBins(cover_reset, GenBin(0,1));
    AddBins(cover_enable, GenBin(0,1));
    AddBins(cover_cycles, GenBin(0,511,4));
    while true loop
      receive(net, coverage_receiver_input, msg);
      input_tran := pop(msg);
      icover(cover_reset, bool2int(input_tran.reset));
      icover(cover_enable, bool2int(input_tran.enable));
      icover(cover_cycles, input_tran.cycles);
    end loop;
  end process;
```

5. Y de manera muy similar crearemos un proceso para calcular el alcance funcional en las transacciones de salida:

```vhdl
  -- Collect functional coverage on output transactions
  coverage_output: process
    variable msg: msg_t;
    variable output_tran: output_tran_t;
  begin
    AddBins(cover_value, GenBin(0,255,4));
    while true loop
      receive(net, coverage_receiver_output, msg);
      output_tran := pop(msg);
      icover(cover_value, output_tran.value);
    end loop;
  end process;
```

Estos procesos indicados son simplemente un ejemplo, y podría tener sentido incrementar el número de bins o incluso calcular el _cross coverage_ (combinaciones de bins en diferentes entradas o salidas). Por ejemplo, para _cycles_ y _value_ se plantean únicamente 4 bins de forma que el log de la simulación quede sencillo, pero tendría sentido aumentar dicho número. Ya que el _functional coverage_ debe escribirlo el ingeniero, las implementaciones específicas dependerán de los criterios que ustedes consideren, no obstante siempre es buena idea plantearse si podemos probar _todos_ los casos posibles, y si no, dividir el espacio de posibilidades en rangos que tenga sentido asegurarnos que hemos probado.

6. Finalmente, modificamos el process `main` (secuencia de test) de forma que se imprima el alcance funcional en el log al final de la simulación, justo antes del `test_runner_cleanup`:

```vhdl
    -- Print functional coverage
    writebin(cover_reset);
    writebin(cover_enable);
    writebin(cover_cycles);
    writebin(cover_value);

    test_runner_cleanup(runner);
```

También se pueden imprimir los 'bins' no alcanzados para cada objeto de coverage con el procedure `WriteCovHoles`.

# Trabajo del alumno

El alumno debe leer el código del testbench que se proporciona y asegurarse de entender los conceptos. También debería ejecutar las simulaciones y mirar los logs de salida de las mismas. Sería buena idea hacer algún cambio en el diseño bajo test o en el predictor para ver la clase de errores que se generan.

Se recomienda también que eche un vistazo al informe de code coverage generado, simplemente abriendo `coverage_report/index.html` con el navegador web.

El alumno debe modificar sus ficheros `run.py` para que se asemejen al que se proporciona, de forma que pueda generar informes de code coverage para el trabajo de la asignatura y también aplicar las funcionalidades de las librerias vistas aquí cuando le sea útil para dicho trabajo.

Para el _functional coverage_, se puede empezar a trabajarlo modificando este testbench o directamente aplicándolo a un testbench del trabajo de la asignatura.

El alumno debería probar estos conceptos, poco a poco, en el testbench de alguno de los módulos del trabajo que tenga que verificar, si bien para el trabajo de la asignatura hay muchas transacciones que son tan sencillas (estilo dato + valid) que no será estrictamente necesario aplicar todo lo visto aquí.

El alumno debe generar un testbench basado en modelado a nivel de transacción para el interpolador que se les proporciona como IP del trabajo. A la hora de realizar este testbench, lo más sencillo es considerar que el interpolador genera 12 transacciones de salida (estilo dato+valid) por cada transacción de entrada.



