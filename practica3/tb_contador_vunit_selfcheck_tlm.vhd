library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library src_lib;

library vunit_lib;
context vunit_lib.vunit_context;

library osvvm;
use osvvm.randompkg.all;

-- Allow the use of VUnit's communication library
context vunit_lib.com_context;

entity tb_contador_vunit_selfcheck_tlm is
  generic (runner_cfg : string);
end;

architecture bench of tb_contador_vunit_selfcheck_tlm is

  -- Clock period
  constant clk_period : time := 5 ns;

  -- Ports
  signal clk : std_ulogic;
  signal rst : std_ulogic;
  signal ena : std_ulogic;
  signal Q : unsigned(7 downto 0);

  -- Input and output transactions
  --
  -- It would be convenient to move the transactions and associated
  -- functions/procedures to a package
  type input_tran_t is record
    reset   : boolean;
    enable  : boolean;
    cycles  : integer;
  end record;

  type output_tran_t is record
    value : integer;  
  end record;

  -- Since VUnit's communication library only allows to push and pop simple
  -- datatypes, we'll define our own procedures to push and pop complete
  -- transactions, using the already-defined push and pop procedures
  -- We will also define our own to_string() functions for debugging purposes
  procedure push(msg: msg_t; input_tran: input_tran_t) is
  begin
    push(msg, input_tran.reset);
    push(msg, input_tran.enable);
    push(msg, input_tran.cycles);
  end procedure;

  -- Impure functions can read and write out of their scope, they have
  -- 'side effects'. Our 'pop' must be impure since it calls VUnit's 'pop'
  -- functions, which are impure themselves
  impure function pop(msg: msg_t) return input_tran_t is
    variable input_tran : input_tran_t;
  begin
    input_tran.reset  := pop(msg);
    input_tran.enable := pop(msg);
    input_tran.cycles := pop(msg);
    return input_tran;
  end function;

  function to_string(input_tran: input_tran_t) return string is
  begin
    return "[input_tran] (reset => " & to_string(input_tran.reset) & ", enable => " & to_string(input_tran.enable) & ", cycles => " & to_string(input_tran.cycles) & ")"; 
  end function;

  procedure push(msg: msg_t; output_tran: output_tran_t) is
  begin
    push(msg, output_tran.value);
  end procedure;

  impure function pop(msg: msg_t) return output_tran_t is
    variable output_tran : output_tran_t;
  begin
    output_tran.value := pop(msg);
    return output_tran;
  end function;

  function to_string(output_tran: output_tran_t) return string is
  begin
    return "[output_tran] (value => " & to_string(output_tran.value) & ")";
  end function;

  -- Message receivers, one for each entity that will receive transactions
  -- The checker has two receivers, so it can easily differentiate between
  -- actual and expected transactions
  constant driver_receiver : actor_t := new_actor("driver receiver");
  constant predictor_receiver : actor_t := new_actor("predictor receiver");
  constant checker_receiver_actual : actor_t := new_actor("checker receiver actual");
  constant checker_receiver_expected : actor_t := new_actor("checker receiver expected");

begin

  -- Uncomment to show PASS messages for passed checks. If commented, will only show check messages for failed checks
  show(get_logger(default_checker), display_handler, pass);

  -- Uncomment to allow a number of errors before stopping the simulation
  -- This is useful so we have some more clk cycles in the waveform after
  -- an error is detected, which helps with debugging
  --set_stop_count(error, 2);

  contador_inst : entity src_lib.contador
    port map (
      clk => clk,
      rst => rst,
      ena => ena,
      Q => Q
    );

  main : process

    variable randomgen : RandomPType;

    -- For each transaction we have to do multiple things, so let's encapsulate:
    --  0. Populate input_tran with the correct fields (input for this procedure)
    --  1. Print the transaction values for debugging
    --  2. Create a new message
    --  3. Push the transaction to the message
    --  4. Make a copy of the message so we can send a copy to the predictor
    --     (the message gets deleted when sent)
    --  5. Send the message with the transaction to the driver
    --  6. Send the copy of the message to the predictor
    --  7. Wait a bit of time so the simulation can run (if not, we would
    --     reach the cleanup function before the circuit could react)
    procedure seq_tran(constant input_tran: in input_tran_t) is
      variable msg : msg_t;
      variable msg_copy : msg_t;
    begin
      info("test_sequence: pushing:" & to_string(input_tran));
      msg := new_msg;
      push(msg, input_tran);
      msg_copy := copy(msg);
      send(net, driver_receiver, msg);
      send(net, predictor_receiver, msg_copy);
      wait for input_tran.cycles*clk_period;
    end procedure;

  begin
    test_runner_setup(runner, runner_cfg);

    while test_suite loop
      if run("test_reset") then
        info("counter: test_reset");
        -- Test sequence for test_reset
        seq_tran((reset => true, enable => false, cycles => 1));
        seq_tran((reset => false, enable => true, cycles => 100));

      elsif run("test_overflow") then
        info("counter: test_overflow");
        -- Test sequence for test_overflow
        seq_tran((reset => true, enable => false, cycles => 1));
        seq_tran((reset => false, enable => true, cycles => 258));

      elsif run("test_alternating_ena") then
        info("counter: test_alternating_ena");
        -- Test sequence for test_alternating_ena
        seq_tran((reset => true, enable => false, cycles => 1));

        for i in 0 to 200 loop
          seq_tran((reset => false, enable => true, cycles => 1));
          seq_tran((reset => false, enable => false, cycles => 1));
        end loop;
      
      end if;

      if run("test_random") then
        info("counter: test_random");
        -- Test sequence for test_random

        -- Initialize random seed
        randomgen.InitSeed(randomgen'instance_name);

        for i in 0 to 20 loop
          seq_tran((reset => randomgen.RandBool, enable => randomgen.RandBool, cycles => randomgen.RandInt(0,511)));
        end loop;

      end if;
    end loop;

    test_runner_cleanup(runner);
  end process main;

  clk_process : process
  begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
  end process clk_process;

  -- Convert input trasactions to pin movement
  driver: process
    variable msg: msg_t;
    variable input_tran: input_tran_t;
  begin
    info("driver: waiting for transaction...");
    -- Wait until a transaction is received
    receive(net, driver_receiver, msg);
    -- Extract the transaction from the message
    input_tran := pop(msg);
    info("driver: received:" & to_string(input_tran));
    -- Drive the UUT pins
    if input_tran.reset then
      rst <= '1';
    else
      rst <= '0';
    end if;
    if input_tran.enable then
      ena <= '1';
    else
      ena <= '0';
    end if;
    --info("driver: waiting for " & to_string(input_tran.cycles) & " cycles");
    for i in 0 to input_tran.cycles loop
      wait for clk_period;
    end loop;
    --info("driver: end of wait");
  end process;

  -- Convert pin movement to output transactions, which is somewhat
  -- direct for the counter: we have an output value each clock cycle
  monitor: process
    variable msg: msg_t;
    variable output_tran: output_tran_t;
  begin
    wait until falling_edge(clk);
    output_tran.value := to_integer(Q);
    info("monitor: detected:" & to_string(output_tran));
    msg := new_msg;
    push(msg, output_tran);
    send(net, checker_receiver_actual, msg);
  end process;

  -- Predict the expected output, now at transaction level
  -- Due to the nature of the counter, and to simplify these transaction-level
  -- models, each input transaction will generate a number of output
  -- transactions (exactly input_tran.cycles transactions)
  -- A more elegant solution, with less output transactions can be implemented,
  -- but it would make both the monitor, predictor and checker more complex
  predictor: process
    variable input_tran: input_tran_t;
    variable expected_output_tran: output_tran_t;
    variable expected_Q: integer;
    variable msg: msg_t;
  begin
    expected_Q := to_integer(Q);
    -- Do everything in an infinite loop because we don't want to exit the
    -- process, since we want to keep the value of the internal variable
    -- expected_Q
    while true loop
      -- Wait until a transaction is received
      receive(net, predictor_receiver, msg);
      -- Extract the transaction from the message
      input_tran := pop(msg);
      info("predictor: input:" & to_string(input_tran));

      -- Send as many output transactions as cycles in the input_tran
      for i in 0 to input_tran.cycles loop
        if input_tran.reset then
          expected_Q := 0;
        elsif input_tran.enable then
          expected_Q := (expected_Q + 1) mod 256;
        end if;
        -- Create the output transaction and send it to the checker
        expected_output_tran.value := expected_Q;
        info("predictor: expected:" & to_string(expected_output_tran));
        msg := new_msg;
        push(msg, expected_output_tran);
        send(net, checker_receiver_expected, msg);
      end loop;
    end loop;
  end process;

  -- Check that circuit outputs match the expected
  checker: process
    variable msg_actual: msg_t;
    variable msg_expected: msg_t;
    variable output_tran_actual: output_tran_t;
    variable output_tran_expected: output_tran_t;
  begin
    -- Wait for both actual and expected output transactions
    receive(net, checker_receiver_actual, msg_actual);
    receive(net, checker_receiver_expected, msg_expected);
    -- Extract the transactions from the message
    output_tran_actual := pop(msg_actual);
    output_tran_expected := pop(msg_expected);
    info("checker: actual:" & to_string(output_tran_actual));
    info("checker: expected:" & to_string(output_tran_expected));
    -- Check transactions match
    check(output_tran_actual.value = output_tran_expected.value, "checker: actual value (" & to_string(output_tran_actual.value) & ") and expected value (" & to_string(output_tran_expected.value) & ") must be equal");
  end process;

end;
