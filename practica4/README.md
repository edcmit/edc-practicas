# Práctica 4

Integración continua para diseños HDL.

## Contenidos

[TOC]

# Objetivos

El objetivo técnico de esta práctica es configurar el sistema de integración continua que proporciona Gitlab, llamado Gitlab CI, de forma que a cada `commit` en el repositorio `git` que haga el alumno, se ejecuten automáticamente todos los tests que tenga definidos. Esto le permitirá encontrar problemas en su código justo en el momento en el que los cambios que los originan se añadan al repositorio remoto.

Con esto se pretenden conseguir los siguientes objetivos formativos:

- Realizar una primera toma de contacto con el concepto de Integración Continua o _Continuous Integration_ (CI)
- Aplicar de manera práctica una buena práctica en el desarrollo de proyectos a un trabajo en curso
- Comprender cómo muchas buenas prácticas utilizadas para desarrollos de software pueden ser muy útiles también para el desarrollo de hardware
- Comprender los conceptos de pipeline, job, runner y artefactos
- Conocer la sintaxis del fichero `yaml` a través del que se configura el CI.
- Ser capaces de generar informes de test en formato JUnit que serán interpretados por Gitlab
- Dotar al alumno de una herramienta útil que pueda acompañarle a lo largo de su vida laboral


# Trabajo previo

Si el alumno tiene al menos un test auto-chequeante que pueda ejecutar desde la línea de comandos, está preparado para hacer esta práctica

# Documentación importante

## Gitlab CI Pipelines

Podemos encontrar una introducción a los 'pipelines' de Gitlab CI en: https://docs.gitlab.com/ee/ci/pipelines/

## Referencia del fichero `.gitlab-ci.yml`

La referencia completa de la sintaxis del fichero `.gitlab-ci.yml` se encuentra en: https://docs.gitlab.com/ee/ci/yaml/

## VUnit y CI

Esta página de la documentación de VUnit indica cómo generar resultados de test en formato JUnit: https://vunit.github.io/ci/intro.html

Según la documentación, lo único que tenemos que hacer es pasarle un argumento especial al fichero `run.py`:

     python run.py --xunit-xml test_output.xml

## Integración de informes de test unitarios en Gitlab CI

Según la documentación de Gitlab CI, se le puede indicar a Gitlab CI qué fichero `xml` contiene los resultados de los tests, de forma que dichos resultados se muestren en formato `html` al visitar la web del proyecto: https://docs.gitlab.com/ee/ci/unit_test_reports.html

# Cómo funciona Gitlab CI ?

Cuando tenemos en el directorio raíz de nuestro repositorio un fichero llamado `.gitlab-ci.yml`, la instancia de gitlab en la que esté nuestro repositorio (en nuestro caso, www.gitlab.com, pero también funciona con instancias privadas) sabe que, a cada commit, debe iniciar los procesos indicados en el fichero, que define lo que se conoce como un `pipeline`. Este nombre es muy apropiado, ya que tendremos diferentes etapas (_stages_), cada una con uno o varios trabajos (_jobs_). Las etapas se ejecutan secuencialmente, y normalmente sólo se pasa a la siguiente etapa si todos los _jobs_ terminan correctamente. Los _jobs_ son ejecutados por _runners_, que son máquinas que están registradas con la instancia de gitlab, con un grupo o con un proyecto. Por razones de seguridad y de portabilidad, normalmente los runners ejecutan los _jobs_ dentro de imágenes _docker_, aunque también es posible que se ejecuten de forma nativa.

De esta forma, si creamos un fichero `.gitlab-ci.yml` en el raíz de nuestro repositorio, habremos configurado la integración continua para el mismo.

_Nota:_ si al crear el proyecto no habéis desmarcado la opción que habilita un análisis estático del código para detectar potenciales problemas de seguridad en software, vuestro repositorio ya tendrá un fichero `.gitlab-ci`, que podréis sobreescribir con el que se proporciona aquí.

# Creando el fichero `.gitlab-ci.yml`

Se proporciona como ejemplo un primer fichero que pueden incluir en su repositorio. Deben asegurarse que el nombre del fichero es correcto, ``.gitlab-ci.yml``.

A continuación se explican las diferentes partes del fichero:

1. `stages`: una o más etapas que se ejecutarán secuencialmente
2. `before_script`: comandos que se ejecutarán antes de cada `job`
3. jobs: cada job tiene su nombre, pertenece a una etapa, se ejecuta en una imagen de docker y tiene un script con los comandos que se ejecutarán. Pueden ver en el ejemplo que el script normalmente es muy sencillo, ya que lo normal es haber automatizado los builds o simulaciones, por lo que desde el fichero de CI simplemente invocaremos a los scripts que ya deberíamos tener preparados. También cada job puede generar artefactos (_artifacts_). El keyword `when` con valor `always` indica que los artefactos se deben almacenar incluso aunque el proceso falle. En el caso de ejecución de tests, siempre querremos tener los _test reports_ como artefactos, pero en la mayoría de trabajos (por ejemplo, al compilar un software) sólo querremos guardar los artefactos si el proceso termina correctamente (y esto es lo que ocurre por defecto). Un artefacto especial es el _test report_ en formato JUnit, que gitlab leerá para mostrar en la web del proyecto.

```yaml
# List of stages, in order of execution
stages:
  - test

# Common commands to be executed before each job
before_script:
  - source /home/salas/fosshdl/env.rc

# Each job:
#   - belongs to a stage
#   - runs inside a docker image (which in our case contains all the software we need)
#   - has a script with the commands to execute
#   - generates artifacts which can be used by other jobs and downloaded by the user
#   - one of those artifacts is the xml report in JUnit format, which is read by gitlab
test_practica3:
  stage: test
  image: registry.gitlab.com/hgpub/fosshdl-dist:edc.2023
  script:
    - cd practica3
    - python3 run.py --xunit-xml test_output.xml
  artifacts:
    when: always
    paths:
      - practica3/coverage_report
      - practica3/test_output.xml
    reports:
      junit:
        - practica3/test_output.xml
```

Los jobs también pueden depender de que otros trabajos hayan terminado correctamente. Por defecto, los jobs reciben todos los artefactos de todas las etapas anteriores, pero esto se puede ajustar usando el keyword `dependencies`.

La imagen de docker en la que se ejecutará el software es un Ubuntu 20.04 con el software de la asignatura instalado, así que si quieren utilizar un software que no esté instalado en dicha imagen, lo único que deben hacer es añadir al `before_script` líneas adicionales con los comandos `apt` o `pip` necesarios para instalar el software que necesiten, al igual que si estuvieran trabajando en la máquina virtual.

_Nota:_ La carpeta 'coverage_report' sólo se generará si han habilitado el 'code coverage' en su script `run.py`, como se indica en la [práctica anterior](practica3).

_Nota:_ Si miran la referencia de la sintaxis de los ficheros `.gitlab-ci.yml`, verán que se pueden desplegar pipelines mucho más complejos que el que se muestra aquí.

## Visualizando los pipelines

Una vez 'pusheado' el fichero `.gitlab-ci.yml` al repositorio, en la web de nuestro proyecto en gitlab, en el menú de la izquierda, podemos hacer click en el botón "CI/CD" para ver los 'pipelines' del proyecto (un pipeline por commit). Si hacemos click en el botón de los 3 puntos verticales que está a la derecha, podemos descargar los artefactos, y si hacemos click en el botón correspondiente a un commit en la columna 'Status' (que puede indicar disintos estados como 'passed' o 'failed'), veremos los detalles del pipeline

![Pipelines](practica4/img/pipelines.png "Pipelines")

En los detalles del pipeline podemos ver el detalle de los 'jobs' ejecutados así como los tests que se han realizado (para poder visualizar los resultados de los test, se debe haber generado el fichero `xml` con formato JUnit y dicho fichero debe estar correctamente indicado en el `.gitlab-ci.yml`.

![Pipeline details](practica4/img/pipelinedetails.png "Pipeline details")

También podemos ver los logs de la ejecución de los distintos trabajos, si hacemos click en 'jobs' y luego en el icono del estado (status) del mismo.

# Trabajo del alumno

El alumno debe adaptar el fichero `.gitlab-ci.yml` a las particularidades de su repositorio, de forma que todos los tests que tenga en dicho repo (o, como mínimo, todos los tests que afecten al trabajo de la asignatura) se ejecuten automáticamente a cada commit.

También es posible, si lo desean, generar automáticamente la [documentación en el pipeline](https://gitlab.com/edcmit/edc-practicas/-/tree/main/faq#puedo-generar-la-documentaci%C3%B3n-t%C3%A9cnica-de-mi-c%C3%B3digo-en-la-integraci%C3%B3n-continua)

En la imagen docker que se proporciona con el software de la asignatura tienen instalado ``octave``: si el código de sus modelos Matlab usa funcionalidades genéricas, debería funcionar en ``octave`` directamente, o sin necesidad de mucha adaptación.

De todas formas, pueden acceder a una imagen de Matlab que he preparado en la instancia de gitlab que tengo en la Universidad. Ya que ustedes no tienen permiso para descargar la imagen, deben indicarle al 'runner' que use la que ya tiene almacenada. Esto se hace definiendo el "pull_policy" en el job para que no intente descargarla:

```yaml
image:
  name: registry.woden.us.es/containers/matlab:R2024a
  pull_policy: never
```

En el propio [``.gitlab-ci.yml``](.gitlab-ci.yml) de este repositorio tienen disponible una primera etapa, ``matlabconcepts``, en la que se utiliza esta imagen en varios jobs para ejecutar comandos de Matlab y ficheros ``.m`` desde el CI. Verán ejemplos de ejecutar ficheros ``.m`` pasándoles argumentos, lo que les puede ser útil por ejemplo para cambiar el modo de 2k a 8k, la modulación, la ruta donde guardar los ficheros de salida... cualquier cosa que necesiten. En los jobs que se proporcionan de ejemplo se crean ficheros ``.m`` sencillos usando el comando ``echo``: no obstante, ustedes deben añadir los ficheros ``.m`` de su modelo de alto nivel al repositorio.